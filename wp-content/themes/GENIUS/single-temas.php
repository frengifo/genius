<?php get_header(); ?>
<section class="temas">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<aside>
					<h3>TEMAS DE AYUDA</h3>
					<ul>
						<li><a href="#">Cambios y devoluciones</a></li>
						<li><a href="#">Tiempo de envío del producto</a></li>
						<li><a href="#">Garantía del producto</a></li>
						<li><a href="#">Cambios y devoluciones</a></li>
						<li><a href="#">Políticas de privacidad</a></li>
						<li><a href="#">Condiciones de uso</a></li>
						<li><a href="#">Delivery</a></li>
						<li><a href="#">Preguntas frecuentes</a></li>
						<li><a href="#">Libro de reclamaciones</a></li>
						<li><a href="#">Guía de tallas</a></li>
					</ul>
					<h3>
						NOSOTROS
					</h3>
					<ul>
						<li><a href="#">Nuestra historia</a></li>
						<li><a href="#">Trabaja en nuestro equipo</a></li>
					</ul>
				</aside>
			</div>
		
	<?php while ( have_posts() ) : the_post(); ?>

			<div class="col-md-9">
				<article>
					<?php the_content(); ?>
				</article>
			</div>

	<?php endwhile; ?>
		</div>
	</div>
</section>
<?php get_footer(); ?>