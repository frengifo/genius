<?php 
	
	add_theme_support( 'post-thumbnails', array( 'product' ) );

	function register_my_menu() {
	  register_nav_menu('header-menu',__( 'Header Menu' ));
	}
	add_action( 'init', 'register_my_menu' );


	remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
	remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

	add_action('woocommerce_before_main_content', 'genius_wrapper_start', 10);
	add_action('woocommerce_after_main_content', 'genius_wrapper_end', 10);

	function genius_wrapper_start() {
			echo '<section id="main">';
	}

	function genius_wrapper_end() {
			echo '</section>';
	}

	add_action( 'after_setup_theme', 'woocommerce_support' );
	function woocommerce_support() {
	    add_theme_support( 'woocommerce' );
	}


	function pertel_scripts() {
		
		
	    

	    if ( is_singular( 'productos' ) || is_tax( 'tipo' ) ) {
			// Add Genericons, used in the main stylesheet.
			wp_enqueue_style( 'fancybox-style', get_template_directory_uri() . '/js/vendor/fancybox/source/jquery.fancybox.css', array(), '2.1.5' );
			wp_enqueue_script( 'fancybox-script', get_template_directory_uri() . '/js/vendor/fancybox/source/jquery.fancybox.pack.js', array(), '2.1.5');
			// Get the post type from the query
		}
	}
	add_action( 'wp_enqueue_scripts', 'pertel_scripts' );

	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

	function woocommerce_template_loop_product_thumbnail(){
		
			$taxonomy_query_var_product_color = get_query_var( 'product_color', false );			

			if ($taxonomy_query_var_product_color) {
				
				
				$query_product_colors = new WP_Query( 
					array( 
						'post_type' => 'product',
						'post_parent' => get_the_id() ,
						'tax_query' => array(
							array(
								'taxonomy' => 'product_color',
								'field'    => 'slug',
								'terms'    => explode(',', $taxonomy_query_var_product_color),
							),
						)
					) 
				);
				while ( $query_product_colors->have_posts() ) : $query_product_colors->the_post();

					the_post_thumbnail("full");

				endwhile;
				// Restore original Post Data
				wp_reset_postdata();


			}else{
				the_post_thumbnail("full");
			}
			/*echo get_the_id();
			echo get_query_var( 'product_cat', 1 );
			echo get_query_var( 'product_color', 1 );
			$product_grouped = get_product( get_the_id() );
			$children = $product_grouped->get_children();
			var_dump($children);*/
		  	/*$args = array(
				'post_parent' => get_the_id(),
				'post_type'   => 'product', 
				'numberposts' => -1,
				'post_status' => 'publish' 
			); */
		   //$children_array = get_children( $args );
		   //var_dump( $children_array );
	}


	class Walker_Simple_Example extends Walker_Category {  

	    function start_lvl(&$output, $depth=1, $args=array()) {  
	        $output .= "\n<ul class=\"filter_products\">\n";  
	    }  

	    function end_lvl(&$output, $depth=0, $args=array()) {  
	        $output .= "</ul>\n";  
	    }  

	    function start_el(&$output, $item, $depth=0, $args=array()) {  
	    	global $wp_query;
	    	$array_all_query_var = $wp_query->query_vars;

	    	//var_dump($array_all_query_var);
	    	/**************************LINEAS********************************/
	    	$taxonomy_query_var = get_query_var( $item->taxonomy, false );

	    	$product_cat = get_query_var( 'product_cat', false );



   			$all_taxonomy = array_keys(get_object_taxonomies( 'product', 'objects' ));

   			$active= $product_cat == $item->slug ? "active":"";

			$output .= "<li class='item ".$active."'><a href='javascript:;' title='".$item->name."' class='".$item->slug."' data-tax='".$item->taxonomy."' data-slug='".$item->slug."'>".$item->name."</a>";
	    }  

	    function end_el(&$output, $item, $depth=0, $args=array()) {  
	        $output .= "</li>\n";  
	    }  
	} 


	add_action('wp_ajax_nopriv_get_products', 'ajax_get_products');
	add_action('wp_ajax_get_products', 'ajax_get_products');

	function ajax_get_products() {

		$colores = $_GET['terms_colores'] ? array( 'taxonomy' => 'pa_colores', 'field'=>'slug','terms' => $_GET['terms_colores']):null;
		$talla = $_GET['terms_tallas'] ? array( 'taxonomy' => 'pa_tallas', 'field'=>'slug','terms' => $_GET['terms_tallas']):null;
		
		//var_dump($colores);
		//var_dump($talla);

		$liquidacion = $_GET['terms_liquidacion'] ? array('taxonomy' => 'liquidacion','field'=>'slug','terms'=> $_GET['terms_liquidacion'] ):null;
		$product_cat = $_GET['terms_categorias'] ? array('taxonomy' => 'product_cat','field'=>'slug','terms'=> $_GET['terms_categorias'] ):null;

		$paged = ( $_GET['paged'] ) ? $_GET['paged'] : 1;

		//var_dump($paged);

		$args = array(
			'post_type' => 'product',
			'posts_per_page' => 12,
			'paged'          => $paged,
			'post_status' => 'any',
			'tax_query' => array(
				'relation' => 'AND',
				$product_cat,
				$colores,
				$talla,
				$liquidacion
				//$talla
			),
			/*'tax_query' => array(
				//$product_cat,
				array('taxonomy' => 'product_cat','field'=>'slug','terms'=> 'elegant')
			)*/
		);
		$productos = new WP_Query( $args );
		
		//var_dump($productos->max_num_pages);

		$gallery = true;

		?>
		<?php if ( $productos->have_posts() ) { ?> 
		<?php $i = 1 ; ?>
			<ul class="products">
				<?php while ( $productos->have_posts() ) : $productos->the_post(); ?>

					<?php if (strpos(get_the_content(),'[gallery') === false){ $gallery = false; }else{ $gallery = true; }?>
					
					<?php if($gallery){ ?>

						<?php $galleries = get_post_galleries(get_the_id(), false);  ?>
						
						<?php 
							
							if ( is_array( $colores) ) {
								
								foreach ($galleries as $key => $gal) {

										if (  in_array($gal['colores'], $_GET['terms_colores']) ) { ?>
											
											<li class="col-md-3 col-sm-3 col-xs-6 box filter-color" id="index-<?php echo $i; ?>">
												<article >
												
													<a href="<?php the_permalink(); ?>" class="img">
														<img src="<?php echo $gal['src'][0]; ?>" alt="" />
													</a>
													<h3>
														<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
													</h3>
													<article><?php the_excerpt();?></article>
													<h4>S/.<?php echo get_post_meta( get_the_id(), '_min_variation_regular_price', true ); ?></h4>
												</article>
											</li>

											<?php if ($i % 4 == 0) { ?>
												<div class="clear"></div>
											<?php } ?>
											<?php $i++; ?>

										<?php } ?>
										
										

								<?php } 

							}else{ ?>

								<li class="col-md-3 col-sm-3 col-xs-6 box" id="index-<?php echo $i; ?>">
									<article >
										
										<a href="<?php the_permalink(); ?>" class="img">
											<?php the_post_thumbnail('full'); ?>
										</a>
										<h3>
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</h3>
										<article><?php the_excerpt();?></article>
										<h4>S/.<?php echo get_post_meta( get_the_id(), '_min_variation_regular_price', true ); ?></h4>
									</article>
								</li>
								<?php if ($i % 4 == 0) { ?>
									<div class="clear"></div>
								<?php } ?>
								<?php $i++; ?>
								
							<?php } 

						  ?>

					<?php }else{ ?>
						<li class="col-md-3 col-sm-3 col-xs-6 box" id="index-<?php echo $i; ?>">
							<article >
								
								<a href="<?php the_permalink(); ?>" class="img">
									<?php the_post_thumbnail('full'); ?>
								</a>
								<h3>
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</h3>
								<article><?php the_excerpt();?></article>
								<h4>S/.<?php echo get_post_meta( get_the_id(), '_min_variation_regular_price', true ); ?></h4>
							</article>
						</li>
						<?php if ($i % 4 == 0) { ?>
							<div class="clear"></div>
						<?php } ?>
						<?php $i++; ?>
						
					<?php } ?>

					
					<?php //$i++; ?>
				<?php endwhile; // end of the loop. ?>
			</ul>
			<nav class="pagination">
				<?php for ($i=1; $i <= $productos->max_num_pages; $i++) { ?>				

				<?php } ?>
				<?php $prev_page = $paged == 1 ? 1: $paged-1; ?>
				<?php $next_page = $paged == $productos->max_num_pages ? $productos->max_num_pages: $paged+1; ?>
				<a href="javascript:;" onclick="nextPage(<?php echo $prev_page; ?>);">
					<img src="<?php echo get_template_directory_uri() ?>/img/prev.png">
				</a>
				<span class="current_page"><?php echo $paged; ?></span> de 
				<span class="max_page"><?php echo $productos->max_num_pages; ?></span>
				<a href="javascript:;" onclick="nextPage(<?php echo $next_page; ?>);">
					<img src="<?php echo get_template_directory_uri() ?>/img/next.png">
				</a>
			</nav>
		<?php }else{ ?> 

			<p class="text-center not-found">No se encontraron productos.</p>

		<?php } ?>

		<?php wp_die(); ?>
<?php
	}

	function custom_override_shipping_fields( $fields ) { 

		$fields['shipping']['shipping_country'] = array(
			'type'      => 'select',
			'label'     => __('País', 'woocommerce'),
			'options' 	=> array('PE' => 'Perú')
		);

		return $fields; 
	} 
	//add_filter( 'woocommerce_checkout_fields' , 'woo_override_checkout_fields' );
	//add_filter( 'woocommerce_billing_fields' , 'woo_override_checkout_fields' );
	//add_filter( 'woocommerce_shipping_fields' , 'custom_override_shipping_fields' );
	add_filter( 'woocommerce_billing_fields' , 'custom_override_billing_fields' );

	function custom_override_billing_fields( $fields ) { 

		$fields['billing_country'] = array(
			'type'      => 'select',
			'label'     => __('País', 'woocommerce'),
			'options' 	=> array('PE' => 'Perú')
		);

		return $fields; 
	} 


	


	add_action('wp_ajax_nopriv_get_subtotal', 'ajax_get_subtotal');
	add_action('wp_ajax_get_subtotal', 'ajax_get_subtotal');

	function ajax_get_subtotal() {
		wc_cart_totals_subtotal_html();
		wp_die();
	}


	add_action('wp_ajax_nopriv_get_cart', 'ajax_get_cart');
	add_action('wp_ajax_get_cart', 'ajax_get_cart');

	function ajax_get_cart() {

		global $woocommerce;
    	$items = $woocommerce->cart->get_cart();
    	$quantity = array();
    	foreach ($items as $key => $value) {
    		//var_dump($value);
    		if ( $value['product_id'] == $_GET['current_id']  &&  $value['variation_id'] == $_GET['current_variation_id'] ) {
    			echo json_encode($value);
    			//var_dump($value);
    			exit();
    		}
    	}
    	//echo json_encode(array("productos" => $items));

    	wp_die();


	}

	add_action('wp_ajax_nopriv_get_search_products', 'ajax_get_search_products');
	add_action('wp_ajax_get_search_products', 'ajax_get_search_products');

	function ajax_get_search_products() {

		$paged = ( $_GET['paged'] ) ? $_GET['paged'] : 1;
		$s = isset($_GET['s']) ? $_GET['s']:"" ;
		//var_dump($paged);

		$args = array(
			'post_type' => 'product',
			'posts_per_page' => 12,
			'paged' => $paged,
			's' =>  isset($_GET['s']) ? $_GET['s']:"" ,
			'order' => 'ASC'
		);
		$productos = new WP_Query( $args );

		//var_dump($productos->max_num_pages);
		?>
		<?php if ( $productos->have_posts() ) { ?>
		<?php $i = 1 ; ?> 
			<ul class="products">
				<?php while ( $productos->have_posts() ) : $productos->the_post(); ?>

					<?php //wc_get_template_part( 'content', 'product' ); ?>
					
					
					<li class="col-md-3 col-sm-3 col-xs-6 box">
						<article >
							<a href="<?php the_permalink(); ?>" class="img">
								<?php the_post_thumbnail('full'); ?>
							</a>
							<h3>
								<a href="<?php the_permalink(); ?>">
									<?php $title = get_the_title(); $keys= explode(" ",$s); $title = preg_replace('/('.implode('|', $keys) .')/iu', '<strong class="search-excerpt">\0</strong>', $title); ?>
	                                <?php echo $title; ?>
								</a>
							</h3>
							<article><?php the_excerpt();?></article>
							<h4>S/.<?php echo get_post_meta( get_the_id(), '_min_variation_regular_price', true ); ?></h4>
						</article>
					</li>
				<?php if ($i % 4 == 0) { ?>
					<div class="clear"></div>
				<?php } ?>
				<?php $i++; ?>
				<?php endwhile; // end of the loop. ?>
			</ul>
			<nav class="pagination">
				<?php for ($i=1; $i <= $productos->max_num_pages; $i++) { ?>				

				<?php } ?>
				<?php $prev_page = $paged == 1 ? 1: $paged-1; ?>
				<?php $next_page = $paged == $productos->max_num_pages ? $productos->max_num_pages: $paged+1; ?>
				<a href="javascript:;" onclick="nextPage(<?php echo $prev_page; ?>);">
					<img src="<?php echo get_template_directory_uri() ?>/img/prev.png">
				</a>
				<span class="current_page"><?php echo $paged; ?></span> de 
				<span class="max_page"><?php echo $productos->max_num_pages; ?></span>
				<a href="javascript:;" onclick="nextPage(<?php echo $next_page; ?>);">
					<img src="<?php echo get_template_directory_uri() ?>/img/next.png">
				</a>
			</nav>
			<?php }else{ ?> 

				<p class="text-center not-found">No se encontraron productos.</p>

			<?php } ?>
		<?php wp_die();

	}


 	add_action('wp_ajax_nopriv_get_all_products', 'ajax_get_all_products');
	add_action('wp_ajax_get_all_products', 'ajax_get_all_products');

	function ajax_get_all_products() {

						// Get
		$lines = get_terms( 'product_cat', array( 'hide_empty' => 0) );
		$linesArray = array();
		foreach ($lines as $key => $value) {
			$arrayName[$key] = $value->slug;
		}
		
		$paged = ( $_GET['paged'] ) ? $_GET['paged'] : 1;

		//var_dump($paged);

		$args = array(
			'post_type' => 'product',
			'posts_per_page' => 12,
			'paged' => $paged,
			'tax_query' => array(
				'relation' => 'AND',
				array('taxonomy' => 'product_cat','field'=>'slug','terms'=> $arrayName )
				//array('taxonomy' => 'pa_tallas','field'=>'slug','terms'=> $arrayName)
			),
			'order' => 'ASC'
		);
		$productos = new WP_Query( $args );

		//var_dump($productos->max_num_pages);
		?>
		<?php if ( $productos->have_posts() ) { ?> 
		<?php $i = 1 ; ?>
			<ul class="products">
				<?php while ( $productos->have_posts() ) : $productos->the_post(); ?>

					<?php //wc_get_template_part( 'content', 'product' ); ?>
					
					
					<li class="col-md-3 col-sm-3 col-xs-6 box">
						<article >
							<a href="<?php the_permalink(); ?>" class="img">
								<?php the_post_thumbnail('full'); ?>
							</a>
							<h3>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</h3>
							<article><?php the_excerpt();?></article>
							<h4>S/.<?php echo get_post_meta( get_the_id(), '_min_variation_regular_price', true ); ?></h4>
						</article>
					</li>
				<?php if ($i % 4 == 0) { ?>
					<div class="clear"></div>
				<?php } ?>
				<?php $i++; ?>
				<?php endwhile; // end of the loop. ?>
			</ul>
			<nav class="pagination">
				<?php for ($i=1; $i <= $productos->max_num_pages; $i++) { ?>				

				<?php } ?>
				<?php $prev_page = $paged == 1 ? 1: $paged-1; ?>
				<?php $next_page = $paged == $productos->max_num_pages ? $productos->max_num_pages: $paged+1; ?>
				<a href="javascript:;" onclick="nextPage(<?php echo $prev_page; ?>);">
					<img src="<?php echo get_template_directory_uri() ?>/img/prev.png">
				</a>
				<span class="current_page"><?php echo $paged; ?></span> de 
				<span class="max_page"><?php echo $productos->max_num_pages; ?></span>
				<a href="javascript:;" onclick="nextPage(<?php echo $next_page; ?>);">
					<img src="<?php echo get_template_directory_uri() ?>/img/next.png">
				</a>
			</nav>

		<?php }else{ ?> 

			<p class="text-center not-found">No se encontraron productos.</p>

		<?php } ?>

		<?php wp_die();

	}


add_action('print_media_templates', function(){

  // define your backbone template;
  // the "tmpl-" prefix is required,
  // and your input field should have a data-setting attribute
  // matching the shortcode name
  ?>
  <script type="text/html" id="tmpl-my-custom-gallery-setting">
    <label class="setting">
      <span><?php _e('Seleccione Color'); ?></span>
      <select data-setting="colores">
        
      	<?php 
      	$terms = get_terms( array(
		    'taxonomy' => 'pa_colores',
		    'hide_empty' => false,
		) );

      	foreach ($terms as $key => $value) { ?>
		
      		<option value="<?php echo $value->slug; ?>"><?php echo $value->name; ?></option>

		<?php 
		}

      	?>

      </select>
    </label>
  </script>

  <script>

    jQuery(document).ready(function(){

      // add your shortcode attribute and its default value to the
      // gallery settings list; $.extend should work as well...
      _.extend(wp.media.gallery.defaults, {
        colores: 'default_val'
      });

      // merge default gallery settings template with yours
      wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
        template: function(view){
          return wp.media.template('gallery-settings')(view)
               + wp.media.template('my-custom-gallery-setting')(view);
        }
      });

    });

  </script>
  <?php

});