<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css?v=<?php echo random_int(0, 1000); ?>">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
    <script src="<?php echo get_template_directory_uri() ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700|Ubuntu+Condensed|Ubuntu' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.7&appId=238310832987958";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
</head>

<body <?php body_class(); ?>>
	
    <header>
        <section class="top-black">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="social pull-left">
                            <span>Síguenos: </span>
                            <a href="https://www.facebook.com/geniusunderwear" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href="https://www.youtube.com/channel/UC9dIZ4VPv__aLVTSoz0Hx4Q/videos" target="_blank"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                        </div>
                        <div class="pull-right">
                            <div class="box-login">
                                <a href="javascript:;" class="login-link ">
                                    <i class="fa fa-user" aria-hidden="true"></i> INGRESA/REGISTRATE 
                                </a>
                                <div class="box-form-login">
                                    <form>
                                        <input type="email" name="email_login" placeholder="Correo electrónico">
                                        <input type="password" name="password_login" placeholder="Contraseña">
                                        <label class="pull-left">
                                            <input type="checkbox" name="" value="1"> Permanecer con la sesión inciada.
                                        </label>
                                        <a href="#" class="recover pull-right">¿Olvidaste tu contraseña</a>
                                        <div class="clear"></div>
                                        <button type="submit">INICIAR SESIÓN</button>
                                        <a href="#" class="">¿Todavía no eres miembro?</a> <a href="<?php echo site_url(); ?>/mi-cuenta/" class="color-yellow obten">Obtén tu cuenta aqui</a>
                                    </form>
                                </div>
                            </div>
                            
                            <a href="<?php echo site_url(); ?>/carro/" class="login-link car-top-icon">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i> <?php wc_cart_totals_subtotal_html(); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="nav">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <a href="<?php echo site_url(); ?>" class="logo"> <img src="<?php echo get_template_directory_uri() ?>/img/logo-genius.png"> </a>
                        <nav class="navigation pull-right">
                            <?php 
                                //wp_nav_menu( array( 'menu' => 'menu-principal' ) ); 
                            ?>
                            <ul>
                                <li>
                                    <a href="<?php echo site_url(); ?>/tienda/" class="hover-products">
                                        PRODUCTOS
                                    </a>
                                    <div class="hover">
                                        <div class="row back">
                                            <div class="col-md-3">
                                                <img src="<?php echo get_template_directory_uri() ?>/img/detail-product.png">
                                            </div>
                                            <div class="col-md-9">
                                                <?php $categories = get_terms( 'product_cat' ); ?>
                                                <?php $i=0; ?>
                                                <?php foreach ($categories as $key => $value): ?>

                                                        <div class="box">
                                                            <h4>
                                                                <?php echo $value->name; ?>
                                                            </h4>
                                                            <ul>
                                                                <?php $products = new WP_Query( array( "product_cat" => $value->slug ) ) ?>
                                                                <?php while ( $products->have_posts() ) : $products->the_post(); ?>

                                                                    <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>


                                                                <?php endwhile; ?>
                                                            </ul>
                                                        </div>
                                                        <?php $i++; ?>
                                                        <?php if ($i % 4 == 0): ?>
                                                            <div class="clear"></div>
                                                        <?php endif ?>
                                                <?php endforeach ?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a href="<?php echo site_url(); ?>/catalogos/">CATALOGOS</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url(); ?>/blog/">BLOG</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url(); ?>/contacto/">CONTACTO</a>
                                </li>
                            </ul>
                            <?php  //var_dump(get_post_ancestors( $post->ID )); ?>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
    </header>