jQuery(document).ready(function( $ ){ 

  $( "a.hover-products, .hover" )
  .mouseenter(function() {
    $("header").css({"z-index":"9999"});
  })
  .mouseleave(function() {
    $("header").css({"z-index":"99"});
  });

	var slider = $(".slider .box").unslider({

		nav : false,
		arrows: false,
		autoplay:true,
		delay:6000,
  	});

  	$(".next").on("click", function(){

  		current_line_index++;

  		if ( li_index == 0) {
  			li_index = li.length-1;
  		}else{
  			li_index--;
  		}
	  	
  		/*$(".selector ul li").removeClass("active_line");
  		$(li[li_index]).addClass("active_line");*/

  		nextLine();

  	})
	
  	$(".prev").on("click", function(){

  		current_line_index--;

  		if ( li_index == li.length-1) {
  			li_index = 0;
  		}else{
  			li_index++;
  		}
	  	
  		/*$(".selector ul li").removeClass("active_line");
  		$(li[li_index]).addClass("active_line");*/

  		nextLine();

  	})

  	$('.slider-full').slick({
    	arrows: false,
		asNavFor: '.slider-thumb',
		slidesToShow: 1,
		fade: true,
    });
    $('.slider-thumb').slick({
    	arrows: false,
    	vertical:false,
    	asNavFor: '.slider-full',
    	slidesToShow: 3,
  		focusOnSelect: true
    });

    
    $(".btn-close").on("click", function(){
    	$(".notify-add-to-car").slideToggle();	
    })

    $(".filter .item a").on("click", function(){
      next_page = 1;
      $(this).parent().toggleClass("active");
      filter_products();


    })

    if ( $(".listado-productos").length > 0 ) {
      if ( $(".categorias li").hasClass("active")) {
          filter_products();
      }else{
        jQuery.get( site_url + '/wp-admin/admin-ajax.php', {

          action: 'get_all_products',
          paged:next_page,
        }, function(data){
           $(".featured-products, .filter").removeClass("loading");
           $(".featured-products").html(data);

        }, 'html');
      }
    }
      

    $(".show-all").on("click", function(){
        next_page = 1;
        show_all();

    })

    $(".view-product .filter .color li a").on("click", function(){

      $(".view-product .filter .color li").removeClass("active");
      $(this).parent().addClass("active");
      current_color =  $(this).data("color");
      current_color_text = $(this).html();
    })

    $(".view-product .filter .tallas li a").on("click", function(){
      
      $(".view-product .filter .tallas li").removeClass("active");
      $(this).parent().addClass("active");
      current_talla =  $(this).data("talla");
      current_talla_text = $(this).html();
    })

    $(".add-to-cart").on("click", function(){

      $(".validate").css({"opacity":"0"});

      if ( $(".color li").hasClass("active") && $(".tallas li").hasClass("active") ) {

        current_variation_id = $("."+current_color+"-"+current_talla).data("id");
        var quantity = $("#quantity").val();
        var url_add_to_cart = '?add-to-cart='+current_id+'&quantity='+quantity+'&variation_id='+current_variation_id+'&attribute_pa_colores='+current_color+'&attribute_pa_tallas='+current_talla;
        console.log(url_add_to_cart);
        $(".box.info").addClass("loading");
        $.get(site_url+url_add_to_cart, function() {
           

            jQuery.get( site_url + '/wp-admin/admin-ajax.php', {

              action: 'get_cart',
              current_id:current_id,
              current_variation_id:current_variation_id

            }, function(data){
              
              $("#prod_quantity").html(data.quantity);
              $("#prod_talla").html(current_talla_text);
              $("#prod_color").html(current_color_text);

              $(".notify-add-to-car").slideToggle();
              $(".view-product .box.info").removeClass("loading");

              updateTopSubtotal();

            }, 'json');

        });

      }else{
        $(".validate").css({"opacity":"1"});
      }

      
    })

    $("#form-search").submit(function( event ) {
      if ( $.trim($("#search").val()) != "" ) {
        search_products();
      }else{
        $("#search").focus();
      }
      event.preventDefault();
    });

    $("#search-product").on("click", function(){
      if ( $.trim($("#search").val()) != "" ) {
        search_products();
      }else{
        $("#search").focus();
      }
    })

})

var next_page = 1;
var block_pagination = "all";

var current_color ="";
var current_talla ="";
var current_variation_id = 0;

var current_talla_text ="";
var current_color_text ="";

var tax = null;


var current_line_index = 0;
var angleStart = -360; // start angle
var li_index = 4;
// jquery rotate animation
function rotate(li,d, i) {
  $({d:angleStart}).animate({d:d}, {
   step: function(now) {
   	$(li).addClass( "li-"+i )
    $(li)
      .css({ transform: 'rotate('+now+'deg)' })
      .find('label')
       .css({ transform: 'rotate('+(-now)+'deg)' });
   }, duration: 0
  });
}
var li = null;
var deg = 0;
// show / hide the options
function loadLines(s) {
  $(s).addClass('open');
  li = $(s).find('li');

  	deg = 360/li.length;
	for(var i=0; i<li.length; i++) {

		var d = i*deg;
		rotate(li[i],d, i)

  	}
}

function updateTopSubtotal(){

  jQuery.get( site_url + '/wp-admin/admin-ajax.php', {

    action: 'get_subtotal',
    
  }, function(data){
      jQuery(".car-top-icon").html(data);

  }, 'html');

}

function nextLine() {
  	
	for(var i=0; i<li.length; i++) {

		var d =(i + current_line_index)*deg;

		//rotate(li[i],d, i)

		$(li[i])
	      .css({ transform: 'rotate('+d+'deg)' })
	      .find('label')
	       .css({ transform: 'rotate('+(-d)+'deg)' });
  	}
  	
  	console.log(li_index );
  	$(".selector ul li").removeClass("active_line");
  	$(li[li_index]).addClass("active_line");
  	$(".desc article").hide();
  	$(".desc article:eq("+li_index+")").fadeIn();
}

function nextPage(paged){
  next_page = paged;
  if (block_pagination == "all") {
    show_all();
  }else{
    filter_products();
  }
}

function show_all(){
  jQuery("#search").val("");
  if (block_pagination == "filter") { next_page = 1;}
  block_pagination = "all";
  $(".filter .item").removeClass("active");
  $(".featured-products, .filter").addClass("loading");
  jQuery.get( site_url + '/wp-admin/admin-ajax.php', {

    action: 'get_all_products',
    paged:next_page,
  }, function(data){
    $(".featured-products, .filter").removeClass("loading");
     $(".featured-products").html(data);

  }, 'html');
}

function search_products(){

  if (block_pagination == "filter") { next_page = 1;}
  block_pagination = "all";
  $(".filter .item").removeClass("active");
  $(".featured-products, .filter").addClass("loading");
  jQuery.get( site_url + '/wp-admin/admin-ajax.php', {

    action: 'get_search_products',
    paged:next_page,
    s: $("#search").val()
  }, function(data){
    $(".featured-products, .filter").removeClass("loading");
     $(".featured-products").html(data);

  }, 'html');
}


function filter_products(){
  jQuery("#search").val("");

  if (block_pagination == "all") { next_page = 1;}
  block_pagination = "filter";

  
  var categorias = jQuery(".categorias li").hasClass("active") ? true:false;
  var colores = jQuery(".color li").hasClass("active") ? true:false;
  var tallas = jQuery(".tallas li").hasClass("active") ? true:false;
  var liquidacion = jQuery(".liquidacion li").hasClass("active") ? true:false;

  var terms_categorias = [];
  if ( categorias ) {

    var items = jQuery(".categorias li.active");

    for (var i = 0; i < items.length; i++) {

      terms_categorias[i] = jQuery(items[i]).find("a").data("slug");

    }

    console.log(terms_categorias);
  }

  var terms_colores = [];
  if ( colores ) {

    var items = jQuery(".color li.active");

    for (var i = 0; i < items.length; i++) {

      terms_colores[i] = jQuery(items[i]).find("a").data("slug");

    }

    console.log(terms_colores);
  }


  var terms_tallas = [];
  if ( tallas ) {

    var items = jQuery(".tallas li.active");

    for (var i = 0; i < items.length; i++) {

      terms_tallas[i] = jQuery(items[i]).find("a").data("slug");

    }

    console.log(terms_tallas);
  }

  var terms_liquidacion = [];
  if ( liquidacion ) {

    var items = jQuery(".liquidacion li.active");

    for (var i = 0; i < items.length; i++) {

      terms_liquidacion[i] = jQuery(items[i]).find("a").data("slug");

    }

    console.log(terms_liquidacion);
  }
  jQuery(".featured-products, .filter").addClass("loading");
  jQuery.get( site_url + '/wp-admin/admin-ajax.php', {

    action: 'get_products',
    categorias: categorias,
    terms_categorias: terms_categorias,
    colores: colores,
    terms_colores: terms_colores,
    tallas: tallas,
    terms_tallas: terms_tallas,
    liquidacion: liquidacion,
    terms_liquidacion: terms_liquidacion,
    paged:next_page,
  }, function(data){
      jQuery(".featured-products, .filter").removeClass("loading");
     jQuery(".featured-products").html(data);

  }, 'html');

}

setTimeout(function() { loadLines('.selector'); }, 100);
