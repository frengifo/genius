<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
		
		<section class="tarjeta cuenta">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<h2 style="border:0;">
							PAGO
						</h2>
						<p>Paga tu pedido con tarjeta de crédito, transferencia bancaria o en efectivo</p>
						<form method="post" action="#">
							<label>
								Nombre(s) del titular*
							</label>
							<input type="text" name="name">
							<label>
								Apellidos(s) del titular*
							</label>
							<input type="text" name="lastname">
							<label>
								Número de tarjeta*
							</label>
							<label>
								<span><input type="radio" name="tipo_tarjeta">Crédito</span>
								<span><input type="radio" name="tipo_tarjeta">Débito</span>
							</label>
							<input type="text" name="tarjeta">
							<label>
								Fecha de expiración*
							</label>
							<div class="row">
								<div class="col-md-6">
									<input type="text" name="fec_a">
								</div>
								<div class="col-md-6">
									<input type="text" name="fec_b">
								</div>
							</div>
							<label>
								Código de seguridad*
							</label>
							<input type="text" name="codigo">

							<a href="<?php echo site_url(); ?>/gracias/" class="btn-finalizar">FINALIZAR COMPRA <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
						</form>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<h2 style="border:0;">&nbsp;</h2>
						<section class="resumen">
							<h4>RESUMEN</h4>
							<span class="pull-right quantity">4 PRODUCTOS</span>
							<div class="box-subtotal">
								<h5>SUBTOTAL</h5>
								<span class="subtotal pull-right">S/.82.00</span>
								<div class="clear"></div>
								<h5>COSTO DE ENVÍO</h5>
								<span class="costo-envio pull-right">S/.15.00</span>
								<div class="clear"></div>
								<h5 class="gratis-envio">
									ENVÍO GRATIS POR <br>LLEVAR 3 Ó MÁS PRODUCTOS
								</h5>
								<span class="gratis-envio text pull-right">S/.15.00</span>
							</div>
							<h3 class="pull-left total">
								TOTAL
								<strong>S/.82.00</strong>
							</h3>
							<a href="<?php echo site_url(); ?>/mi-cuenta/" class="btn-continue pull-left">CONTINUAR <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
						</section>
						<div class="help">
	                        <h4>CONFIRMA TU DIRECCIÓN DE ENVÍO</h4>
							<p>Urb. San Luis 4454, Dpt0. 402 <br> San Borja - Lima Perú</p>
							<a href="#" class="edit-dir">EDITAR</a>
						</div>
					</div>
					<div class="col-md-12">
						<p>&nbsp;</p>
						<p><img src="<?php echo get_template_directory_uri() ?>/img/iconos-seguridad.png" width="100%"></p>
					</div>
				</div>

			</div>
		</section>
		
<?php endwhile; ?>
<?php get_footer(); ?>