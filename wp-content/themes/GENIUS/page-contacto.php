<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
		<section class="slider">
			<div class="box">
				<ul>
					<li>
						<img src="<?php echo get_template_directory_uri() ?>/img/header-contacto.png">
					</li>
					
				</ul>
			</div>
		</section>
		<section class="cuenta contacto">
			
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<h2>CONTÁCTANOS</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eius</p>
						<form>
							<input type="text" name="nombre" placeholder="Nombres*">
							<input type="email" name="email" placeholder="Correo electrónico*">
							<input type="text" name="phone" placeholder="Teléfono*">
							<textarea name="msg" placeholder="Mensaje*"></textarea>
							<label>
								<input type="checkbox" name="suscribirme" value="1"> Deseo subscribirme al boletín y recibir información de Genius
							</label>
							<div class="clear"></div>
							<button type="button" class="btn-enviar-contact">ENVIAR</button>
						</form>
					</div>
					<div class="col-md-6">
						<article>
							<ul class="contact">
                                <li class="marker">
                                    Av. Berlin 1501 O 102<br>
                                    Miraflores - Lima - Perú
                                </li>
                                <li class="phone">(01) 456 1327</li>
                                <li class="email">
                                    informes@genius.pe
                                </li>
                            </ul>
                            <div class="gracias">
                            	<a href="javascript:;" class="fb">
									<i class="fa fa-facebook" aria-hidden="true"></i> Facebook
								</a><br>
								<a href="javascript:;" class="tw">
									<i class="fa fa-twitter" aria-hidden="true"></i> Twitter
								</a>
                            </div>
						</article>
					</div>
				</div>
			</div>

		</section>
		
<?php endwhile; ?>
<?php get_footer(); ?>