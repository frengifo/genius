<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

    <section class="blog-interna">
    	
    	<div class="container">
    		<ul class="share-product">
				<li>
					<a href="#" class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				</li>
				<li>
					<a href="#" class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
				</li>
				<li>
					<a href="#" class="gplus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
				</li>
			</ul>
    		<div class="row">

    			<div class="col-md-9 col-sm-9">
    				<article>
    					<h1>
    						Dalor ipsum dolor sit amet tus consecteutur
    						<small>Por <strong>Javier Guzmán</strong> | 13 de Junio, 2016</small>
    					</h1>
    					<p><img src="<?php echo get_template_directory_uri() ?>/img/contenido.png"></p>
    					<p><strong>Lorem ipsum dolor</strong> sit amet, consectetur adipisicing elit, sed do eiusmod
    					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
    					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
    					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    					<p><img src="<?php echo get_template_directory_uri() ?>/img/contenido2.png"></p>
    					<p><img src="<?php echo get_template_directory_uri() ?>/img/contenido3.png" class="alignright"></p>
    					<p>
    						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    						consequat. Duis aute irure dolor in reprehenderit in voluptate velit ess
    					</p>
    					<p>
    						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
    						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    						consequat. Duis aute irure dolor in reprehenderit in voluptate velit ess
    					</p>
    					<p> quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
    						consequat. Dvelit ess
    					</p>
    				</article>
    			</div>
    			<div class="col-md-3 col-md-3">
    				<section class="blog-section">
			
						
						<div class="row">
							<div class="col-md-12">
								<h2>Temas <br>en común</h2>
							</div>
							<div class="col-md-12">
								<div class="row">
									
									<div class="col-md-12 box">
										<article>
											
											<img src="<?php echo get_template_directory_uri() ?>/img/post2.png">
											<a href="#"></a>
											<h2>
												UN BOXER ARRIBA DEL PANTALÓN
												<small>19 julio 2016 - Gabriel Yance</small>
											</h2>
										</article>
									</div>
									<div class="col-md-12 box">
										<article>
											
											<img src="<?php echo get_template_directory_uri() ?>/img/post2.png">
											<a href="#"></a>
											<h2>
												UN BOXER ARRIBA DEL PANTALÓN
												<small>19 julio 2016 - Gabriel Yance</small>
											</h2>
										</article>
									</div>
									<div class="col-md-12 box">
										<article>
											
											<img src="<?php echo get_template_directory_uri() ?>/img/post2.png">
											<a href="#"></a>
											<h2>
												UN BOXER ARRIBA DEL PANTALÓN
												<small>19 julio 2016 - Gabriel Yance</small>
											</h2>
										</article>
									</div>
									<div class="col-md-12 box">
										<article>
											
											<img src="<?php echo get_template_directory_uri() ?>/img/post2.png">
											<a href="#"></a>
											<h2>
												UN BOXER ARRIBA DEL PANTALÓN
												<small>19 julio 2016 - Gabriel Yance</small>
											</h2>
										</article>
									</div>
								</div>
							</div>
						</div>
						

					</section>
    			</div>
    		</div>
    	</div>
    </section>

<?php endwhile; ?>
<?php get_footer(); ?>