<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<section class="cuenta">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</section>

<?php endwhile; ?>
<?php get_footer(); ?>