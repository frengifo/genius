<?php get_header(); ?>
	
	<section class="slider">
		<div class="box">
			<ul>
				<li>
					<img src="<?php echo get_template_directory_uri() ?>/img/banner.jpg">
					<div class="text">
						<p>
							SIEMPRE HAY UN <br>
							<strong>GENIUS</strong><br>
							PARA CADA MOMENTO
						</p>
						<a href="#" class="btn-slider">VER LOS PRODUCTOS</a>
					</div>
				</li>
				<li>
					<img src="<?php echo get_template_directory_uri() ?>/img/banner.jpg">
					<div class="text">
						<p>
							SIEMPRE HAY UN <br>
							<strong>GENIUS</strong><br>
							PARA CADA MOMENTO
						</p>
						<a href="#" class="btn-slider">VER LOS PRODUCTOS</a>
					</div>
				</li>
			</ul>
		</div>
	</section>

	<section class="lines">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>				LÍNEAS
					</h2>
					<div class="selector">
						<div class="desc">
							<article>
								<h3>URBAN </h3>
								<p>Molde anatómico <br> <strong>Estampado Tacto cero</strong> </p>
								<a href="<?php echo site_url(); ?>/linea/urban" class="ver">VER</a>
							</article>
							<article>
								<h3>SPORT </h3>
								<p>Molde anatómico <br> <strong>Estampado Tacto cero</strong> </p>
								<a href="<?php echo site_url(); ?>/linea/sport" class="ver">VER</a>
							</article>
							<article>
								<h3>ELEGANT </h3>
								<p>Molde anatómico <br> <strong>Estampado Tacto cero</strong> </p>
								<a href="<?php echo site_url(); ?>/linea/elegant" class="ver">VER</a>
							</article>
							<article>
								<h3>SUMMER </h3>
								<p>Molde anatómico <br> <strong>Estampado Tacto cero</strong> </p>
								<a href="<?php echo site_url(); ?>/linea/summer" class="ver">VER</a>
							</article>
							<article>
								<h3>SPORT </h3>
								<p>Molde anatómico <br> <strong>Estampado Tacto cero</strong> </p>
								<a href="<?php echo site_url(); ?>/linea/sport" class="ver">VER</a>
							</article>
							<article>
								<h3>ELEGANT </h3>
								<p>Molde anatómico <br> <strong>Estampado Tacto cero</strong> </p>
								<a href="<?php echo site_url(); ?>/linea/elegant" class="ver">VER</a>
							</article>
						</div>
						<a href="javascript:;" class="arrows-lineas prev">
							<img src="<?php echo get_template_directory_uri() ?>/img/prev.png">
						</a>
						<a href="javascript:;" class="arrows-lineas next">
							<img src="<?php echo get_template_directory_uri() ?>/img/next.png">
						</a>
						<ul>
						  <li>
						    <input type="checkbox">
						    <label><a href="<?php echo site_url(); ?>/linea/urban"><img src="<?php echo get_template_directory_uri() ?>/img/l5.png"></a></label>
						  </li>
						  <li>
						    <input type="checkbox">
						    <label><a href="<?php echo site_url(); ?>/linea/sport"><img src="<?php echo get_template_directory_uri() ?>/img/l2.png"></a></label>
						  </li>
						  <li>
						    <input type="checkbox">
						    <label><a href="<?php echo site_url(); ?>/linea/elegant"><img src="<?php echo get_template_directory_uri() ?>/img/l3.png"></a></label>
						  </li>
						  <li>
						    <input type="checkbox">
						    <label><a href="<?php echo site_url(); ?>/linea/summer"><img src="<?php echo get_template_directory_uri() ?>/img/l4.png"></a></label>
						  </li>
						  <li>
						    <input type="checkbox">
						    <label><a href="<?php echo site_url(); ?>/linea/sport"><img src="<?php echo get_template_directory_uri() ?>/img/l6.png"></a></label>
						  </li>
						  <li>
						    <input type="checkbox">
						    <label><a href="<?php echo site_url(); ?>/linea/elegant"><img src="<?php echo get_template_directory_uri() ?>/img/l1.png"></a></label>
						  </li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="atributes">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>
						ATRIBUTOS
					</h2>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="#">
						<img src="<?php echo get_template_directory_uri() ?>/img/linea1.png">
					</a>
					<h3>
						<a href="#">FIBRAS ESPECIALES</a>
					</h3>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="#">
						<img src="<?php echo get_template_directory_uri() ?>/img/linea2.png">
					</a>
					<h3>
						<a href="#">ALGODÓN PIMA</a>
					</h3>
				</div>
				<div class="clear hidden-desktop"></div> 
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="#">
						<img src="<?php echo get_template_directory_uri() ?>/img/linea3.png">
					</a>
					<h3>
						<a href="#">ELÁSTICO MICROFIBRA</a>
					</h3>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="#">
						<img src="<?php echo get_template_directory_uri() ?>/img/linea4.png">
					</a>
					<h3>
						<a href="#">ALGONDÓN SPANDEX</a>
					</h3>
				</div>
			</div>
		</div>
	</section>

	<section class="featured-products">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>
						PRODUCTOS DESTACADOS
					</h2>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="<?php echo site_url(); ?>/tienda/bull" class="img">
						<img src="<?php echo get_template_directory_uri() ?>/img/p1.jpg">
					</a>
					<h3>
						<a href="<?php echo site_url(); ?>/tienda/bull">GENIUS SPORT</a>
					</h3>
					<p>Estampado tacto cero</p>
					<h4>S/. 20.00</h4>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="<?php echo site_url(); ?>/tienda/marine" class="img">
						<img src="<?php echo get_template_directory_uri() ?>/img/p2.jpg">
					</a>
					<h3>
						<a href="<?php echo site_url(); ?>/tienda/marine">GENIUS ELEGANT</a>
					</h3>
					<p>Estampado tacto cero</p>
					<h4>S/. 25.00</h4>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="<?php echo site_url(); ?>/tienda/urbano" class="img">
						<img src="<?php echo get_template_directory_uri() ?>/img/p3.jpg">
					</a>
					<h3>
						<a href="<?php echo site_url(); ?>/tienda/urbano">GENIUS URBANO</a>
					</h3>
					<p>Estampado tacto cero</p>
					<h4>S/. 25.00</h4>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="<?php echo site_url(); ?>/tienda/arc" class="img">
						<img src="<?php echo get_template_directory_uri() ?>/img/p4.jpg">
					</a>
					<h3>
						<a href="<?php echo site_url(); ?>/tienda/arc">GENIUS SUMMER</a>
					</h3>
					<p>Estampado tacto cero</p>
					<h4>S/. 22.00</h4>
				</div>
			</div>
		</div>
	</section>

	<section class="catalogo">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul>
						<li>
							<a href="#">
								<img src="<?php echo get_template_directory_uri() ?>/img/catalogo.jpg">
							</a>
						</li>
						<li>
							<a href="#">
								<img src="<?php echo get_template_directory_uri() ?>/img/blog.jpg">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

<?php get_footer(); ?>