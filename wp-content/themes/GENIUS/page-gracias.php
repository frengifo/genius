<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
		
		<section class="gracias">
			
			<h2>GRACIAS POR TU COMPRA</h2>
			<p>
				Robertho, te hemos enviado un correo de confirmación a tu correo.<br>
				Tu número de transacción es: <strong>89564548888</strong>
			</p>
			<p>
				<img src="<?php echo get_template_directory_uri() ?>/img/check-compra.png">
			</p>

			<h4>
				TE OFRECEMOS UN 5% DE DESCUENTO EN TU PRÓXIMA COMPRA.<br>
				¡SÓLO DEBES COMPARTIR!
			</h4>

			<a href="javascript:;" class="fb">
				<i class="fa fa-facebook" aria-hidden="true"></i> Facebook
			</a>
			<a href="javascript:;" class="tw">
				<i class="fa fa-twitter" aria-hidden="true"></i> Twitter
			</a>

		</section>
		
<?php endwhile; ?>
<?php get_footer(); ?>