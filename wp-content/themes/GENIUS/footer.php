			<footer>
                <section class="newsletter">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="box-newsletter pull-right">
                                    <?php echo do_shortcode( '[contact-form-7 id="1299" title="Suscriber"]' ); ?>
                                </div>

                                <h3>SUSCRÍBETE AL BOLETÍN Y OBTÉN <strong class="discount">15%</strong> DE DESCUENTO</h3>
                                
                            </div>
                        </div>
                    </div>
                </section>
                <section class="foot">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12 box-logo-black">

                                <a href="#" class="logo-black">
                                    <img src="<?php echo get_template_directory_uri() ?>/img/logo-genius-black.png" style="margin-top: -.6em;">
                                </a>
                                <article>
                                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua.</p>
                                </article>
                                <div class="news-form">
                                    <h3>SUSCRÍBETE AHORA</h3>
                                    <form>
                                        <input type="email" name="email" placeholder="Ingresa tu correo eléctronico"><br>
                                        
                                    </form> 
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <h3>NOSOTROS</h3>
                                <ul>
                                    <li>
                                        <a href="#">Nuestra historia</a>  
                                    </li>
                                    <li>
                                        <a href="#">Trabaja en nuestro equipo</a>  
                                    </li>
                                </ul>
                                <h3>SERVICIO AL CLIENTE</h3>
                                <ul>
                                    <li>
                                        <a href="#">Cambios y devoluciones</a>  
                                    </li>
                                    <li>
                                        <a href="#">Tiempo de envío del producto</a>  
                                    </li>
                                    <li>
                                        <a href="#">Garatía del Producto</a>  
                                    </li>
                                    <li>
                                        <a href="#">Políticas de privacidad</a>  
                                    </li>
                                    <li>
                                        <a href="#">Condicions de Uso</a>  
                                    </li>
                                    <li>
                                        <a href="#">Delivery</a>  
                                    </li>
                                    <li>
                                        <a href="#">Preguntas Frecuentes</a>  
                                    </li>
                                    <li>
                                        <a href="#">Libro de reclamaciones</a>  
                                    </li>
                                    <li>
                                        <a href="#">Guía de tallas</a>  
                                    </li>
                                </ul>
                            </div>

                            <div class="col-md-2 col-sm-4 col-xs-12">
                                <h3>LÍNEAS</h3>
                                <ul>
                                    <li>
                                        <a href="#">Elegant</a>  
                                    </li>
                                    <li>
                                        <a href="#">Urban</a>  
                                    </li>
                                    <li>
                                        <a href="#">Sport</a>  
                                    </li>
                                    <li>
                                        <a href="#">Fusoón</a>  
                                    </li>
                                    <li>
                                        <a href="#">Microfibra</a>  
                                    </li>
                                    <li>
                                        <a href="#">Basic</a>  
                                    </li>
                                    <li>
                                        <a href="#">Camisetas</a>  
                                    </li>
                                    <li>
                                        <a href="#">Kids</a>  
                                    </li>
                                </ul>
                                <h3>MEDIOS DE PAGO</h3>
                                <div class="medios">
                                    <span>
                                        <img src="<?php echo get_template_directory_uri() ?>/img/visa.png" alt="Visa">
                                    </span>
                                    <span>
                                        <img src="<?php echo get_template_directory_uri() ?>/img/master-card.png" alt="Mastercard">
                                    </span>
                                    <span>
                                        <img src="<?php echo get_template_directory_uri() ?>/img/america-express.png" alt="American Express">
                                    </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <h3>REDES SOCIALES</h3>
                                <div class="social">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
                                </div>
                                <h3>CONTÁCTANOS</h3>
                                <ul class="contact">
                                    <li class="marker">
                                        Av. Berlin 1501 O 102<br>
                                        Miraflores - Lima - Perú
                                    </li>
                                    <li class="phone">(01) 456 1327</li>
                                    <li class="email">
                                        informes@genius.pe
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="copy">
                	<p>Copyright 2016 <a href="#">GENIUS</a> | Diseñado y Desarrollado por <a href="#">Reder Design</a></p>
                </div>				
            </footer>

        </div>
        <?php wp_footer(); ?>
        
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/vendor/slidebars.min.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/plugins.js"></script>
        <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/main.js?v=3"></script>
        <script type="text/javascript">var site_url ="<?php echo site_url(); ?>";</script>
    </body>
</html>