<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

	<section class="slider">
		<div class="box">
			<ul>
				<li>
					<img src="<?php echo get_template_directory_uri() ?>/img/genius-catalogo.png">
				</li>
				
			</ul>
		</div>
	</section>
	<section class="catalogos">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
					<article class="main-catalogo">
						<img src="<?php echo get_template_directory_uri() ?>/img/main-catalogo.png">
						<a href="<?php echo site_url(); ?>/catalogo/catalogo-1/">
							
						</a>
					</article>
					<article>
						<img src="<?php echo get_template_directory_uri() ?>/img/cat1.png">
						<a href="<?php echo site_url(); ?>/catalogo/catalogo-1/">
							
						</a>
					</article>
					<article>
						<img src="<?php echo get_template_directory_uri() ?>/img/cat2.png">
						<a href="<?php echo site_url(); ?>/catalogo/catalogo-1/">
							
						</a>
					</article>
					<div class="clear"></div>
					<article>
						<img src="<?php echo get_template_directory_uri() ?>/img/cat3.png">
						<a href="<?php echo site_url(); ?>/catalogo/catalogo-1/">
							
						</a>
					</article>
					<article>
						<img src="<?php echo get_template_directory_uri() ?>/img/cat4.png">
						<a href="<?php echo site_url(); ?>/catalogo/catalogo-1/">
							
						</a>
					</article>
					<article>
						<img src="<?php echo get_template_directory_uri() ?>/img/cat5.png">
						<a href="<?php echo site_url(); ?>/catalogo/catalogo-1/">
							
						</a>
					</article>
					<article>
						<img src="<?php echo get_template_directory_uri() ?>/img/cat6.png">
						<a href="<?php echo site_url(); ?>/catalogo/catalogo-1/">
							
						</a>
					</article>
					<article>
						<img src="<?php echo get_template_directory_uri() ?>/img/cat7.png">
						<a href="<?php echo site_url(); ?>/catalogo/catalogo-1/">
							
						</a>
					</article>
					<article>
						<img src="<?php echo get_template_directory_uri() ?>/img/cat8.png">
						<a href="<?php echo site_url(); ?>/catalogo/catalogo-1/">
							
						</a>
					</article>
				</div>
			</div>
		</div>
	</section>
<?php endwhile; ?>
<?php get_footer(); ?>