<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
		
		<section class="cuenta registro">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>REGISTRO</h2>
						<h4>TUS DATOS PERSONALES</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
						<form method="post" action="#">
							<div class="row">
								<div class="col-md-4"><input type="text"  name="name" placeholder="Nombres*"></div>
								<div class="col-md-4"><input type="text"  name="name" placeholder="Apellidos*"></div>
								<div class="col-md-4"><input type="text"  name="name" placeholder="Tipo de documento*"></div>
								<div class="col-md-4"><input type="text"  name="name" placeholder="Fecha de Nacimiento dd/mm/aa*"></div>
								<div class="col-md-4"><input type="text"  name="name" placeholder="Telefono*"></div>
								<div class="col-md-4"><input type="text"  name="name" placeholder="Numero de documento*"></div>
							</div>
						</form>

						<h4>TU UBICACIÓN</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
						<form method="post" action="#">
							<div class="row">
								<div class="col-md-4"><input type="text"  name="name" placeholder="Departamento*"></div>
								<div class="col-md-4"><input type="text"  name="name" placeholder="Provincia*"></div>
								<div class="col-md-4"><input type="text"  name="name" placeholder="Distrito*"></div>
								<div class="col-md-12"><input type="text"  name="name" placeholder="Dirección*"></div>
							</div>
						</form>

						<h4>TUS DATOS DE INICIO DE SESIÓN</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
						<form method="post" action="#">
							<div class="row">
								<div class="col-md-4"><input type="text"  name="name" placeholder="Correo eletrónico*"></div>
								<div class="col-md-4"><input type="text"  name="name" placeholder="Contraseña*"></div>
								<div class="col-md-4"><input type="text"  name="name" placeholder="Repetir contraseña*"></div>
							</div>
						</form>

						<div class="legal">
							<p>
								Doy mi consentimiento a GENIUS para que utilice mis datos personales con fines comerciales y para sondeos de opinión. En particular, GENIUS estará autorizado a analizar mis interacciones con la compañía (como por ejemplo mi historial de compras, el uso de aplicaciones, redes sociales y datos personales que comparto con adidas) para así poderme enviar mensajes comerciales personalizados sobre los productos de adidas. adidas podrá ponerse en contacto conmigo mediante correos electrónicos, mensajes de texto, correo ordinario, aplicaciones o cualquier otro medio de comunicación que yo desee utilizar.
							</p>
						</div>
						<form method="post" action="#">
							<div class="row">
								<div class="col-md-12">
									<input type="checkbox" id="tems"  name="name" value="acepto">
									<label for="terms">Acepto recibir información de Genius y haber aceptado los <strong>Términos y Condiciones y Políticas de Privacidad.</strong></label>
								</div>
								
							</div>
						</form>

						<a href="<?php echo site_url(); ?>/tarjeta/" class="btn-siguiente">SIGUIENTE</a>
						
					</div>
				</div>
			</div>
		</section>
		
<?php endwhile; ?>
<?php get_footer(); ?>