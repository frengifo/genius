<?php get_header(); ?>
<section class="carro">
	<?php while ( have_posts() ) : the_post(); ?>
		
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				<?php 
								//print_r($packages); ?>
				<?php wc_print_notices(); ?>
				

					<h1>TU CARRITO  <small><?php echo WC()->cart->cart_contents_count; ?> PRODUCTOS</small></h1>
					
				</div>
				<div class="col-md-8">
					<section class="head-cart">
						<div class="col-md-7 col-sm-7  col-xs-6"></div>
						<div class="col-md-5 col-sm-5  col-xs-6">
							<div class="col-md-4 col-sm-4 col-xs-4">
								<p>Unidad</p>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<p>Cantidad</p>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<p>Total</p>
							</div>
						</div>
					</section>

					<section class="list-cart">
						<form action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

							<?php do_action( 'woocommerce_before_cart_table' ); ?>

								
								<?php do_action( 'woocommerce_before_cart_contents' ); ?>

								<?php
								foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
									$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
									$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

									if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
										$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
										?>
										<article class="<?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
											<div class="row">
												<div class="col-md-7 col-sm-7  col-xs-6">
													<div class="row">
														<div class="col-md-4 col-sm-4  col-xs-4">

															<?php
																echo $_product->get_image();
															?>
															
														</div>
														<div class="col-md-8 col-sm-8  col-xs-8">
															<div class="product-name" data-title="<?php _e( 'Product', 'woocommerce' ); ?>">
																<h3>
																	<?php echo $_product->get_title(); ?>
																</h3>
																<p><strong>Cód.: <?php echo $_product->get_sku(); ?></strong></p>
																	<?php // Meta data
																	echo  ( WC()->cart->get_item_data( $cart_item ) );

																	// Backorder notification
																	if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
																		echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>';
																	}
																?>
															</div>
															<?php
																echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
																	'<a href="%s" class="delete-product-cart" title="%s" data-product_id="%s" data-product_sku="%s">ELIMINAR</a>',
																	esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
																	__( 'Remove this item', 'woocommerce' ),
																	esc_attr( $product_id ),
																	esc_attr( $_product->get_sku() )
																), $cart_item_key );
															?>
														</div>
													</div>
												</div>
												<div class="col-md-5 col-sm-5  col-xs-6">
													<div class="row">
														<div class="col-md-4 col-sm-4 col-xs-4">
															<h4>
																<?php
																	echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
																?>
															</h4>
														</div>
														<div class="col-md-4 col-sm-4 col-xs-4">
															<?php
																if ( $_product->is_sold_individually() ) {
																	$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
																} else {
																	$product_quantity = woocommerce_quantity_input( array(
																		'input_name'  => "cart[{$cart_item_key}][qty]",
																		'input_value' => $cart_item['quantity'],
																		'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
																		'min_value'   => '1'
																	), $_product, false );
																}

																echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
															?>
														</div>
														<div class="col-md-4 col-sm-4 col-xs-4">
															<h4>
																<?php
																	echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
																?>
															</h4>
														</div>
													</div>
												</div>
											</div>
										</article>
										<?php
									}
								}

								do_action( 'woocommerce_cart_contents' ); ?>

								<article>

								<input type="submit" class="edit-product-cart pull-right" name="update_cart" value="ACTUALIZAR" />
									

										

										

										<?php do_action( 'woocommerce_cart_actions' ); ?>

										<?php wp_nonce_field( 'woocommerce-cart' ); ?>
								</article>

								<?php do_action( 'woocommerce_after_cart_contents' ); ?>

							<?php do_action( 'woocommerce_after_cart_table' ); ?>

						</form>

					</section>
				</div>
				
				<div class="col-md-4 col-sm-4">
					<h5 class="seguir-comprando">
						<a href="<?php echo site_url(); ?>/tienda/"><i class="fa fa-shopping-cart" aria-hidden="true"></i> SEGUIR COMPRANDO </a>
					</h5>
					<section class="resumen">
						<h4>RESUMEN</h4>
						<span class="pull-right quantity"><?php echo WC()->cart->cart_contents_count; ?> PRODUCTOS</span>
						<div class="box-subtotal">
							<h5>SUBTOTAL</h5>
							<span class="subtotal pull-right"><?php wc_cart_totals_subtotal_html(); ?></span>

							<div class="clear"></div>
							<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
								<h5><?php wc_cart_totals_coupon_label( $coupon ); ?></h5>
								<span class="pull-right"><?php wc_cart_totals_coupon_html( $coupon ); ?></span>

							<?php endforeach; ?>
							<div class="clear"></div>
							<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
								<?php do_action( 'woocommerce_cart_totals_before_shipping' ); ?>

								<?php wc_cart_totals_shipping_html(); ?>

								<?php do_action( 'woocommerce_cart_totals_after_shipping' ); ?>

							<?php elseif ( WC()->cart->needs_shipping() && 'yes' === get_option( 'woocommerce_enable_shipping_calc' ) ) : ?>

								<div class="shipping">
									<h5><?php _e( 'Shipping', 'woocommerce' ); ?></h5>
									<span class="pull-right" data-title="<?php esc_attr_e( 'Shipping', 'woocommerce' ); ?>"><?php woocommerce_shipping_calculator(); ?></span>
								</div>

							<?php endif; ?>
							<div class="clear"></div>
							<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
								<div class="fee">
									<h5><?php echo esc_html( $fee->name ); ?></h5>
									<span class="pull-right" data-title="<?php echo esc_attr( $fee->name ); ?>"><?php wc_cart_totals_fee_html( $fee ); ?></span>
								</div>
							<?php endforeach; ?>
							<div class="clear"></div>
							<?php if ( wc_tax_enabled() && 'excl' === WC()->cart->tax_display_cart ) :
								$taxable_address = WC()->customer->get_taxable_address();
								$estimated_text  = WC()->customer->is_customer_outside_base() && ! WC()->customer->has_calculated_shipping()
										? sprintf( ' <small>(' . __( 'estimated for %s', 'woocommerce' ) . ')</small>', WC()->countries->estimated_for_prefix( $taxable_address[0] ) . WC()->countries->countries[ $taxable_address[0] ] )
										: '';

								if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
									<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
										<div class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
											<h5><?php echo esc_html( $tax->label ) . $estimated_text; ?></h5>
											<span class="pull-right" data-title="<?php echo esc_attr( $tax->label ); ?>"><?php echo wp_kses_post( $tax->formatted_amount ); ?></span>
										</div>
									<?php endforeach; ?>
								<?php else : ?>
									<div class="tax-total">
										<h5><?php echo esc_html( WC()->countries->tax_or_vat() ) . $estimated_text; ?></h5>
										<span class="pull-right" data-title="<?php echo esc_attr( WC()->countries->tax_or_vat() ); ?>"><?php wc_cart_totals_taxes_total_html(); ?></span>
									</div>
								<?php endif; ?>
							<?php endif; ?>

							<?php $minQuantity_freeShipping = 3; ?>
														
							<h5>COSTO DE ENVÍO</h5>
							<span class="costo-envio pull-right">S/.15.00</span>
							<div class="clear"></div>

							<?php if (WC()->cart->cart_contents_count >= $minQuantity_freeShipping ): ?>
								<h5 class="gratis-envio">
									ENVÍO GRATIS POR <br>LLEVAR 3 Ó MÁS PRODUCTOS
								</h5>
								<span class="gratis-envio text pull-right">S/.15.00</span>
							<?php endif ?>
								
							
						</div>
						<h3 class="pull-left total">
							<?php do_action( 'woocommerce_cart_totals_before_order_total' ); ?>
							<?php _e( 'TOTAL', 'woocommerce' ); ?> <span class="pull-right">
								<?php if (WC()->cart->cart_contents_count < $minQuantity_freeShipping  ){ ?>
									<?php echo wc_price( (WC()->cart->subtotal + 15) ); ?>
								<?php }else{ ?> 
									<?php echo wc_price( WC()->cart->cart_contents_total ); ?>
								<?php } ?>
								
							</span>
							<?php do_action( 'woocommerce_cart_totals_after_order_total' ); ?>
						</h3>
						<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
						
					</section>
					<?php if ( wc_coupons_enabled() ) { ?>
						<div class="coupon">
							<h3>CÓDIGOS PROMOCIONAL</h3>
							<input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'woocommerce' ); ?>" /> 
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit,
							</p>
							<input type="submit" class="button" name="apply_coupon" value="APLICAR" />
							<?php do_action( 'woocommerce_cart_coupon' ); ?>
						</div>
					<?php } ?>
					<div class="help">
						<h3>MEDIOS DE PAGO</h3>
						<div class="medios">
                            <span>
                                <img src="<?php echo get_template_directory_uri() ?>/img/visa.png" alt="Visa">
                            </span>
                            <span>
                                <img src="<?php echo get_template_directory_uri() ?>/img/master-card.png" alt="Mastercard">
                            </span>
                            <span>
                                <img src="<?php echo get_template_directory_uri() ?>/img/america-express.png" alt="American Express">
                            </span>
                        </div>
                        <h3>¿NECESITAS AYUDA?</h3>
						<p>Tiempo de llegada de la orden <br> Garantía del producto <br> Cambios y devoluciones</p>
						<p><strong>Llámanos al 0800-77533</strong></p>
						<p>De lunes a viernes de 9:00 a 21:00<br> y sábados de 10:00 a 18:00</p>
					</div>
				</div>
			</div>
		</div>
			
	<?php endwhile; ?>
</section>

<section class="featured-products no-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>
						PRODUCTOS DESTACADOS
					</h2>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="#" class="img">
						<img src="<?php echo get_template_directory_uri() ?>/img/p1.jpg">
					</a>
					<h3>
						<a href="#">GENIUS SPORT</a>
					</h3>
					<p>Estampado tacto cero</p>
					<h4>S/. 20.00</h4>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="#" class="img">
						<img src="<?php echo get_template_directory_uri() ?>/img/p2.jpg">
					</a>
					<h3>
						<a href="#">GENIUS ELEGANT</a>
					</h3>
					<p>Estampado tacto cero</p>
					<h4>S/. 25.00</h4>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="#" class="img">
						<img src="<?php echo get_template_directory_uri() ?>/img/p3.jpg">
					</a>
					<h3>
						<a href="#">GENIUS URBANO</a>
					</h3>
					<p>Estampado tacto cero</p>
					<h4>S/. 25.00</h4>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="#" class="img">
						<img src="<?php echo get_template_directory_uri() ?>/img/p4.jpg">
					</a>
					<h3>
						<a href="#">GENIUS SUMMER</a>
					</h3>
					<p>Estampado tacto cero</p>
					<h4>S/. 22.00</h4>
				</div>
			</div>
		</div>
	</section>
<?php get_footer(); ?>