<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
		
		<section class="cuenta">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php wc_print_notices(); ?>
						<?php the_content(); ?>
					</div>
					<!--<?php if ( !is_user_logged_in() ) { ?>
						<div class="col-md-8">

							<?php //the_content(); ?>
							<h2>REGISTRO</h2>
							<h4>TUS DATOS PERSONALES</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>

							<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

								<?php if ( !is_user_logged_in() ) { ?>
									
								
									<form method="post" class="register">

										<?php do_action( 'woocommerce_register_form_start' ); ?>

										

										<input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>" />


										<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

											<p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
												<label for="reg_password"><?php _e( 'Password', 'woocommerce' ); ?> <span class="required">*</span></label>
												<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" />
											</p>

										<?php endif; ?>

										<div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php _e( 'Anti-spam', 'woocommerce' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

										<?php do_action( 'woocommerce_register_form' ); ?>
										<?php do_action( 'register_form' ); ?>

										<p class="woocomerce-FormRow form-row">
											<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
											<input type="submit" class="btn-siguiente" name="register" value="REGISTRAR" />
										</p>

										<?php do_action( 'woocommerce_register_form_end' ); ?>

									</form>

								<?php } ?>

							<?php endif; ?>
						</div>
						<div class="col-md-4">
							<section>
								<h2>INICIAR SESIÓN</h2>
								<div class="login-form">
									<h4>INGRESA TUS DATOS</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
									
									<form method="post" class="login">

										<?php do_action( 'woocommerce_login_form_start' ); ?>

										<input type="text" placeholder="Nombre de usuario o Correo electrónico" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />

										<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />

										<?php do_action( 'woocommerce_login_form' ); ?>

										<p class="form-row">
											<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
											<input type="submit" class="woocommerce-Button button btn-iniciar" name="login" value="INICIAR SESIÓN" />
											<label for="rememberme" class="inline">
												<input class="woocommerce-Input woocommerce-Input--checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php _e( 'Remember me', 'woocommerce' ); ?>
											</label>
										</p>
										<p class="woocommerce-LostPassword lost_password">
											<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php _e( 'Lost your password?', 'woocommerce' ); ?></a>
										</p>

										<?php do_action( 'woocommerce_login_form_end' ); ?>

									</form>
								</div>
							</section>
						</div>
					<?php }else{ ?> 
						
						<div class="col-md-3">
							
							<?php do_action( 'woocommerce_account_navigation' ); ?>

						</div>
						<div class="col-md-9">
							<div class="">
								<?php
									/**
									 * My Account content.
									 * @since 2.6.0
									 */
									do_action( 'woocommerce_account_content' );
								?>
							</div>
						</div>

					<?php } ?>-->
				</div>
			</div>
		</section>
		
<?php endwhile; ?>
<?php get_footer(); ?>