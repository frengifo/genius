<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
		
		<section class="cuenta pormayor">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>VENTAS POR MAYOR</h2>
						<p>
							Doy mi consentimiento a Genius para que utilice mis datos personales con fines comerciales y para sondeos de opinión. En particular, GENIUS estará autorizado a analizar mis interacciones con comerciales personalizados sobre los productos de Genius.
						</p>
						<section class="box">
							<h4>INFORMACIÓN GENERAL</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
							<form method="post" action="#">
								<div class="row">
									<div class="col-md-4"><input type="text"  name="name" placeholder="Nombres o razon social*"></div>
									<div class="col-md-4"><input type="text"  name="name" placeholder="Tipo de documento*"></div>
									<div class="col-md-4"><input type="text"  name="name" placeholder="Numero de documento*"></div>
								</div>
							</form>

							<h4>INFORMACIÓN DE CONTACTO Y UBICACIÓN</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
							<form method="post" action="#">
								<div class="row">
									<div class="col-md-4"><input type="text"  name="name" placeholder="Correo electrónico*"></div>
									<div class="col-md-4"><input type="text"  name="name" placeholder="Celular*"></div>
									<div class="col-md-4"><input type="text"  name="name" placeholder="Teléfono*"></div>
									<div class="col-md-4"><input type="text"  name="name" placeholder="Departamento*"></div>
									<div class="col-md-4"><input type="text"  name="name" placeholder="Provincia*"></div>
									<div class="col-md-4"><input type="text"  name="name" placeholder="Distrito*"></div>
									<div class="col-md-12"><input type="text"  name="name" placeholder="Dirección*"></div>
								</div>
							</form>

							<h4>DEJA TU MENSAJE</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
							<form method="post" action="#">
								<div class="row">
									<div class="col-md-12">
										<textarea></textarea>
									</div>
								</div>
							</form>
							<a href="<?php echo site_url(); ?>/" class="btn-solicitar-cita">SOLICITAR UNA CITA</a>
						</section>
					</div>
				</div>
			</div>
		</section>
		
<?php endwhile; ?>
<?php get_footer(); ?>