<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<section class="slider">
	<div class="box">
		<ul>
			<li>
				<img src="<?php echo get_template_directory_uri() ?>/img/banner_1.jpg">
			</li>
		</ul>
	</div>
</section>

	<div class="container">
		<div class="row">
			
		</div>
	</div>
</section>
<section class="tienda">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<section class="box-breadcrumbs">
					<?php
						/**
						 * woocommerce_before_main_content hook.
						 *
						 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
						 * @hooked woocommerce_breadcrumb - 20
						 */
						do_action( 'woocommerce_before_main_content' );
					?>
				</section>
			</div>
			<div class="col-md-3">
				<div class="box-form-search">
					<form action="#" method="get" id="form-search">
						<input type="text" id="search" name="search" placeholder="Buscar los productos"> <button id="search-product" type="button"> <img src="<?php echo get_template_directory_uri() ?>/img/search-icon.png"></button>
					</form>
				</div>
				<section class="filter loading">
					<a href="javascript:;" class="show-all pull-right">Ver todo</a>
					<h2>FILTROS</h2>
                    <h3>LÍNEAS</h3>
                    
                    <ul class="categorias">
                    	<?php 
	                    	wp_list_categories( array( 
	                    		'taxonomy' => 'product_cat', 
	                    		'hide_empty' => 0, 
	                    		'title_li' => __( '' ) ,
	                    		'walker'=> new Walker_Simple_Example
	                    		) 
	                    	);
	                    ?>
                    </ul>
                    <h3>COLOR</h3>
                   	<ul class="color">
                    	<?php 
	                    	wp_list_categories( array( 
	                    		'taxonomy' => 'pa_colores', 
	                    		'hide_empty' => 0, 
	                    		'title_li' => __( '' ) ,
	                    		'walker'=> new Walker_Simple_Example
	                    		) 
	                    	);
	                    ?>
                    </ul>
                    <h3>TALLA</h3>
                    <ul class="tallas">
                    	<?php 
	                    	wp_list_categories( array( 
	                    		'taxonomy' => 'pa_tallas', 
	                    		'hide_empty' => 0, 
	                    		'title_li' => __( '' ) ,
	                    		'walker'=> new Walker_Simple_Example
	                    		) 
	                    	);
	                    ?>
                    </ul>
                    <h3>LIQUIDACIÓN</h3>
                    <ul class="liquidacion">
                    	<?php 
	                    	wp_list_categories( array( 
	                    		'taxonomy' => 'liquidacion', 
	                    		'hide_empty' => 0, 
	                    		'title_li' => __( '' ) ,
	                    		'walker'=> new Walker_Simple_Example
	                    		) 
	                    	);
	                    ?>

                    </ul>

				</section>
				<div class="help">
					<h3>¿NECESITAS AYUDA?</h3>
					<p>Tiempo de llegada de la orden <br> Garantía del producto <br> Cambios y devoluciones</p>
					<p><strong>Llámanos al 0800-77533</strong></p>
					<p>De lunes a viernes de 9:00 a 21:00<br> y sábados de 10:00 a 18:00</p>
				</div>
			</div>
			<div class="col-md-9 listado-productos">
				<div class="row featured-products loading">
					


					
					
				</div>
						

				<?php /*---------------------------------------------------*/ ?>

				<?php
				/**
				* woocommerce_after_shop_loop hook.
				*
				* @hooked woocommerce_pagination - 10
				*/
				do_action( 'woocommerce_after_shop_loop' );
				?>

				<?php //elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

				<?php //wc_get_template( 'loop/no-products-found.php' ); ?>

				<?php //endif; ?>

				<?php
				/**
				* woocommerce_after_main_content hook.
				*
				* @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
				*/
				do_action( 'woocommerce_after_main_content' );
				?>

					
			</div>
		</div>
	</div>
</section>
<?php get_footer( 'shop' ); ?>
