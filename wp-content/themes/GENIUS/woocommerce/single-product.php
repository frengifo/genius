<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<?php while ( have_posts() ) : the_post(); ?>
	<section class="view-product">
		<div class="container">
			<div class="notify-add-to-car">
				<a href="javascript:;" class="btn-close"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
				<div class="row">
					<div class="col-md-12">
						<h3> PRODUCTO AGREGADO </h3>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<img src="<?php echo get_template_directory_uri() ?>/img/detail-product-thumb-1.png">
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<h4><?php the_title(); ?></h4>
						<div class="sku"><strong>Cod: <?php echo get_post_meta( get_the_id(), '_sku',true ); ?></strong></div>
						<p>
							Color: <span id="prod_color">Azul</span> <br>
							Talla:  <span id="prod_talla">L</span>  <br>
							Cantidad: <span id="prod_quantity"></span> unidad
						</p>
						<h5>
							<a href="<?php echo site_url(); ?>/tienda/"><i class="fa fa-shopping-cart" aria-hidden="true"></i> SEGUIR COMPRANDO </a>
						</h5>
						<a href="<?php echo site_url(); ?>/carro/" class="ver-carrito">VER CARRITO</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<a href="#" class="back">Atras</a>
					<?php
						/**
						 * woocommerce_before_main_content hook.
						 *
						 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
						 * @hooked woocommerce_breadcrumb - 20
						 */
						do_action( 'woocommerce_before_main_content' );
					?>
				</div>
				<div class="col-md-12">
					

						<?php //wc_get_template_part( 'content', 'single-product' ); ?>
						<script type="text/javascript"> var current_id = <?php the_id(); ?>;</script>
					
						<ul class="share-product">
							<li>
								<a href="#" class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a>
							</li>
							<li>
								<a href="#" class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
							</li>
							<li>
								<a href="#" class="gplus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
							</li>
						</ul>
						<div class="screen-full box">
							<article class="color vistas-azul">
								<div class="slider-full">
									<div><img src="<?php echo get_template_directory_uri() ?>/img/detail-product.png"></div>
									<div><img src="<?php echo get_template_directory_uri() ?>/img/detail-product.png"></div>
									<div><img src="<?php echo get_template_directory_uri() ?>/img/detail-product.png"></div>
								</div>
								<div class="slider-thumb">
									<div><img src="<?php echo get_template_directory_uri() ?>/img/detail-product-thumb-1.png"></div>
									<div><img src="<?php echo get_template_directory_uri() ?>/img/detail-product-thumb-2.png"></div>
									<div><img src="<?php echo get_template_directory_uri() ?>/img/detail-product-thumb-3.png"></div>
								</div>
							</article>
						</div>
						<div class="info box">
							<article>
								<h1><?php the_title(); ?></h1>
								<span class="sku">Cód.: <?php echo get_post_meta( get_the_id(), '_sku',true ); ?></span>
								<div class="price"><strong>S/.<?php echo get_post_meta( get_the_ID(), '_max_variation_regular_price', true); ?></strong></div>
								<article>
									<?php the_excerpt(); ?>
								</article>
								<div class="filter">
									<ul class="color">
										<?php $colores = wp_get_post_terms(get_the_id(), 'pa_colores', array("fields" => "all"));?>
										<?php foreach ($colores as $key => $value): ?>
											<li>
					                    		<a href="javascript:;" title="<?php echo $value->slug; ?>" data-color="<?php echo $value->slug; ?>" class="<?php echo $value->slug; ?>">
					                    			<?php echo $value->name ?>
					                    		</a>
					                    	</li>
										<?php endforeach ?>
					                    	
				                    	

				                    </ul>
				                    <ul class="tallas">
				                    	<?php $tallas = wp_get_post_terms(get_the_id(), 'pa_tallas', array("fields" => "all"));?>
										<?php foreach ($tallas as $key => $value): ?>
											<li>
					                    		<a href="javascript:;" data-talla="<?php echo $value->slug; ?>" class="<?php echo $value->slug; ?>">
					                    			<?php echo $value->name ?>
					                    		</a>
					                    	</li>
										<?php endforeach ?>
				                    </ul>
				                    <a href="<?php echo site_url(); ?>/temas/guia-de-tallas/" class="guia">Guía de tallas</a>
				                    <div class="clear"></div>
				                    <form method="get" action="#">
				                    	<input type="number" name="cantidad" id="quantity" min="1" placeholder="Cantidad (1)" value="1">
				                    	<span class="validate" style="opacity:0;">Para agregar al carrito debe seleccionar Color y Talla.</span>
				                    	<button type="button" class="add-to-cart">
				                    		<i class="fa fa-shopping-cart" aria-hidden="true"></i> AÑADIR A CARRITO
				                    	</button>
				                    </form>
				                    
				                    <ul class="variations">
				                    	
				                    
				                    <?php 

				                    	$prod = wc_get_product( get_the_id() );

										$variations = $prod->get_available_variations();



										foreach ($variations as $key => $item) { ?>
											
											<li class="<?php echo $item['attributes']['attribute_pa_colores']."-".$item['attributes']['attribute_pa_tallas'] ?>"
												data-id="<?php echo $item['variation_id']; ?>">
											
											</li>

										<?php } ?>

									</ul>
									<script type="text/javascript"></script> 

								</div>	
							</article>
						</div>

					
				</div>
				<?php
					/**
					 * woocommerce_after_main_content hook.
					 *
					 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
					 */
					//do_action( 'woocommerce_after_main_content' );
				?>

				<?php
					/**
					 * woocommerce_sidebar hook.
					 *
					 * @hooked woocommerce_get_sidebar - 10
					 */
					//do_action( 'woocommerce_sidebar' );
				?>
			</div>
		</div>
	</section>
<?php endwhile; // end of the loop. ?>
	<section class="featured-products no-bg">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>
						PRODUCTOS DESTACADOS
					</h2>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="#" class="img">
						<img src="<?php echo get_template_directory_uri() ?>/img/p1.jpg">
					</a>
					<h3>
						<a href="#">GENIUS SPORT</a>
					</h3>
					<p>Estampado tacto cero</p>
					<h4>S/. 20.00</h4>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="#" class="img">
						<img src="<?php echo get_template_directory_uri() ?>/img/p2.jpg">
					</a>
					<h3>
						<a href="#">GENIUS ELEGANT</a>
					</h3>
					<p>Estampado tacto cero</p>
					<h4>S/. 25.00</h4>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="#" class="img">
						<img src="<?php echo get_template_directory_uri() ?>/img/p3.jpg">
					</a>
					<h3>
						<a href="#">GENIUS URBANO</a>
					</h3>
					<p>Estampado tacto cero</p>
					<h4>S/. 25.00</h4>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-6 box">
					<a href="#" class="img">
						<img src="<?php echo get_template_directory_uri() ?>/img/p4.jpg">
					</a>
					<h3>
						<a href="#">GENIUS SUMMER</a>
					</h3>
					<p>Estampado tacto cero</p>
					<h4>S/. 22.00</h4>
				</div>
			</div>
		</div>
	</section>
<?php get_footer( 'shop' ); ?>
