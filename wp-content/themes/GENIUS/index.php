<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
		<section class="slider">
			<div class="box">
				<ul>
					<li>
						<img src="<?php echo get_template_directory_uri() ?>/img/blog-header.png">
					</li>
				</ul>
			</div>
		</section>
		<section class="blog-section">
			
			<div class="container">
				<ul class="share-product">
					<li>
						<a href="#" class="fb"><i class="fa fa-facebook" aria-hidden="true"></i></a>
					</li>
					<li>
						<a href="#" class="tw"><i class="fa fa-twitter" aria-hidden="true"></i></a>
					</li>
					<li>
						<a href="#" class="gplus"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
					</li>
				</ul>
				<div class="row">
					<div class="col-md-12">
						
						<ul class="pull-left categories">
							<li><a href="#">Categoria 1</a></li>
							<li><a href="#">Categoria 2</a></li>
							<li><a href="#">Categoria 3</a></li>
							<li><a href="#">Categoria 4</a></li>
						</ul>
						<form class="pull-right search-blog">
							<input type="text" class="pull-left" name="search" placeholder="Busca novedades">
							<button type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
						</form>
					</div>
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-8 col-sm-8 col-xs-12 box-main">
								<article>
									
									<img src="<?php echo get_template_directory_uri() ?>/img/post1.png">
									<a href="<?php echo site_url() ?>/entrada-del-blog/"></a>
									<h2>
										COMO MOSTRAR TU BOXER ARRIBA DEL PANTALÓN
										<small>19 julio 2016 - Gabriel Yance</small>
									</h2>
								</article>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-6 box">
								<article>
									
									<img src="<?php echo get_template_directory_uri() ?>/img/blog1.png">
									<a href="<?php echo site_url() ?>/entrada-del-blog/"></a>
									<h2>
										UN BOXER ARRIBA DEL PANTALÓN
										<small>19 julio 2016 - Gabriel Yance</small>
									</h2>
								</article>
							</div>
							<div class="clear"></div>
							<div class="col-md-4 col-sm-4 col-xs-6 box">
								<article>
									
									<img src="<?php echo get_template_directory_uri() ?>/img/blog2.png">
									<a href="<?php echo site_url() ?>/entrada-del-blog/"></a>
									<h2>
										UN BOXER ARRIBA DEL PANTALÓN
										<small>19 julio 2016 - Gabriel Yance</small>
									</h2>
								</article>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-6 box">
								<article>
									
									<img src="<?php echo get_template_directory_uri() ?>/img/blog3.png">
									<a href="<?php echo site_url() ?>/entrada-del-blog/"></a>
									<h2>
										UN BOXER ARRIBA DEL PANTALÓN
										<small>19 julio 2016 - Gabriel Yance</small>
									</h2>
								</article>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-6 box">
								<article>
									
									<img src="<?php echo get_template_directory_uri() ?>/img/blog4.png">
									<a href="<?php echo site_url() ?>/entrada-del-blog/"></a>
									<h2>
										UN BOXER ARRIBA DEL PANTALÓN
										<small>19 julio 2016 - Gabriel Yance</small>
									</h2>
								</article>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-6 box">
								<article>
									
									<img src="<?php echo get_template_directory_uri() ?>/img/blog5.png">
									<a href="<?php echo site_url() ?>/entrada-del-blog/"></a>
									<h2>
										UN BOXER ARRIBA DEL PANTALÓN
										<small>19 julio 2016 - Gabriel Yance</small>
									</h2>
								</article>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-6 box">
								<article>
									
									<img src="<?php echo get_template_directory_uri() ?>/img/blog6.png">
									<a href="<?php echo site_url() ?>/entrada-del-blog/"></a>
									<h2>
										UN BOXER ARRIBA DEL PANTALÓN
										<small>19 julio 2016 - Gabriel Yance</small>
									</h2>
								</article>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-6 box">
								<article>
									
									<img src="<?php echo get_template_directory_uri() ?>/img/blog7.png">
									<a href="<?php echo site_url() ?>/entrada-del-blog/"></a>
									<h2>
										UN BOXER ARRIBA DEL PANTALÓN
										<small>19 julio 2016 - Gabriel Yance</small>
									</h2>
								</article>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-6 box">
								<article>
									
									<img src="<?php echo get_template_directory_uri() ?>/img/blog8.png">
									<a href="<?php echo site_url() ?>/entrada-del-blog/"></a>
									<h2>
										UN BOXER ARRIBA DEL PANTALÓN
										<small>19 julio 2016 - Gabriel Yance</small>
									</h2>
								</article>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-6 box">
								<article>
									
									<img src="<?php echo get_template_directory_uri() ?>/img/blog9.png">
									<a href="#"></a>
									<h2>
										UN BOXER ARRIBA DEL PANTALÓN
										<small>19 julio 2016 - Gabriel Yance</small>
									</h2>
								</article>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-6 box">
								<article>
									
									<img src="<?php echo get_template_directory_uri() ?>/img/post2.png">
									<a href="#"></a>
									<h2>
										UN BOXER ARRIBA DEL PANTALÓN
										<small>19 julio 2016 - Gabriel Yance</small>
									</h2>
								</article>
							</div>
						</div>
					</div>
				</div>
			</div>

		</section>
		<section class="others">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<h2>SÍGUENOS EN FACEBOOK</h2>
						<div class="fb-page" data-href="https://www.facebook.com/geniusunderwear/?fref=ts" data-tabs="timeline" data-height="226" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/geniusunderwear/?fref=ts" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/geniusunderwear/?fref=ts">GENIUS UNDERWEAR</a></blockquote></div>
					</div>
					<div class="col-md-8">
						<section class="featured-products no-bg">
							<div class="row">
								<div class="col-md-12">
									<h2>
										PRODUCTOS DESTACADOS
									</h2>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-6 box">
									<a href="#" class="img">
										<img src="<?php echo get_template_directory_uri() ?>/img/p1.jpg">
									</a>
									<h3>
										<a href="#">GENIUS SPORT</a>
									</h3>
									<p>Estampado tacto cero</p>
									<h4>S/. 20.00</h4>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-6 box">
									<a href="#" class="img">
										<img src="<?php echo get_template_directory_uri() ?>/img/p2.jpg">
									</a>
									<h3>
										<a href="#">GENIUS ELEGANT</a>
									</h3>
									<p>Estampado tacto cero</p>
									<h4>S/. 25.00</h4>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-6 box">
									<a href="#" class="img">
										<img src="<?php echo get_template_directory_uri() ?>/img/p3.jpg">
									</a>
									<h3>
										<a href="#">GENIUS URBANO</a>
									</h3>
									<p>Estampado tacto cero</p>
									<h4>S/. 25.00</h4>
								</div>
								
							</div>
						</section>
					</div>
				</div>
			</div>
		</section>
<?php endwhile; ?>
<?php get_footer(); ?>