<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'db_genius');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'R-^!K.>3&M{l8R,f}T>e!^f=r%9C03vt=BDCX$(&ji?6s[t`[y;0}g3lkl7*);c.');
define('SECURE_AUTH_KEY', 'a,SoXlW7GQ19g0fLrQx.Eu*tN-]-M>{2xnuMbP{SJA<K;F[EJtZ+?vboXFm.?g/T');
define('LOGGED_IN_KEY', 'nI.#y*[9Kl_|~r=BN!w[FIY%=AOtu$qPXAi(tN9h:.jJz&KU[&L*MOEl;I+4Lbg_');
define('NONCE_KEY', ',*bQ=SNGM*]|$LRd7unKbjU2)~=891]74=$)P)uEQ>9!it3jlz{`B2!vFZ6NCp@)');
define('AUTH_SALT', 'v&l0]e.#z[6k01~:JMJ{#3N`.-{$@n CB:4+)nv4T`MU6?u6:#g=J.sp2s0ti@,!');
define('SECURE_AUTH_SALT', 'InSU:W|QpI}t($YFHrcG5F!LD|wM UhPz2H5H~#R#ti_HdH}}<v#nF{usv:fMqJP');
define('LOGGED_IN_SALT', 'vwOI3]Iv-<odv|w_wLcuN-=sXIo4`G^<,Y#9m0?3Zh|^|lHP<mv9ze[HK*C&&MRL');
define('NONCE_SALT', '!= 3]!7%<ZIJ@Kfd!*++0*eA7J%IfzJqzYOKDZCRhz<(p| k-b>q$+nx/Q^8q/l2');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wpgen_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

