<?php include "includes/header.php"; ?>  

  <section>
    <div class="c-elastic">
      <div class="c-mbot-sm c-img-bgcover c-coverslide" style="background-image: url(img/proyectos-entregados.png);">
        <div class="c-box-text c-color-blackt1">
          <p class="c-h2 c-titi-bol">GRANDES HISTORIAS<br>EN EL MEJOR LUGAR PARA VIVIR</p>
          <p class="c-h3 c-titi">CONOCE NUESTROS<br><span class="c-titi-sem">PROYECTOS ENTREGADOS</span></p>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row c-mtop-xs">
        <div class="col-sm-4 c-mbot-sm">
          <div class="c-proy-otro">
            <a href="proyectos-entregados-detalle.php"><figure><img src="img/proy-entre1.png" alt=""></figure></a>
            <div class="c-proy-text">
              <a href="proyectos-entregados-detalle.php" class="c-h2 c-color-sklight c-titi-sem">Miramar</a>
              <p class="c-h4 c-color-sklight c-titi"><i>Av. del Pilar 1501 - Surco</i></p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat impedit placeat natus beatae ut, rerum maxime.</p>
            </div>
            <div class="row c-bg-sklight c-color-white c-feat-full text-center c-titi">
              <div class="col-xs-4">
                ÁREA<br>80m<sup>2</sup> 
              </div>
              <div class="col-xs-4">
                PISOS<br>25 
              </div>
              <div class="col-xs-4">
                DPTOS.<br>250 
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4 c-mbot-sm">
          <div class="c-proy-otro">
            <a href="proyectos-entregados-detalle.php"><figure><img src="img/proy-entre1.png" alt=""></figure></a>
            <div class="c-proy-text">
              <a href="proyectos-entregados-detalle.php" class="c-h2 c-color-sklight c-titi-sem">Miramar</a>
              <p class="c-h4 c-color-sklight c-titi"><i>Av. del Pilar 1501 - Surco</i></p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat impedit placeat natus beatae ut, rerum maxime.</p>
            </div>
            <div class="row c-bg-sklight c-color-white c-feat-full text-center c-titi">
              <div class="col-xs-4">
                ÁREA<br>80m<sup>2</sup> 
              </div>
              <div class="col-xs-4">
                PISOS<br>25 
              </div>
              <div class="col-xs-4">
                DPTOS.<br>250 
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4 c-mbot-sm">
          <div class="c-proy-otro">
            <a href="proyectos-entregados-detalle.php"><figure><img src="img/proy-entre1.png" alt=""></figure></a>
            <div class="c-proy-text">
              <a href="proyectos-entregados-detalle.php" class="c-h2 c-color-sklight c-titi-sem">Miramar</a>
              <p class="c-h4 c-color-sklight c-titi"><i>Av. del Pilar 1501 - Surco</i></p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat impedit placeat natus beatae ut, rerum maxime.</p>
            </div>
            <div class="row c-bg-sklight c-color-white c-feat-full text-center c-titi">
              <div class="col-xs-4">
                ÁREA<br>80m<sup>2</sup> 
              </div>
              <div class="col-xs-4">
                PISOS<br>25 
              </div>
              <div class="col-xs-4">
                DPTOS.<br>250 
              </div>
            </div>
          </div>
        </div>
                <div class="col-sm-4 c-mbot-sm">
          <div class="c-proy-otro">
            <a href="proyectos-entregados-detalle.php"><figure><img src="img/proy-entre1.png" alt=""></figure></a>
            <div class="c-proy-text">
              <a href="proyectos-entregados-detalle.php" class="c-h2 c-color-sklight c-titi-sem">Miramar</a>
              <p class="c-h4 c-color-sklight c-titi"><i>Av. del Pilar 1501 - Surco</i></p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat impedit placeat natus beatae ut, rerum maxime.</p>
            </div>
            <div class="row c-bg-sklight c-color-white c-feat-full text-center c-titi">
              <div class="col-xs-4">
                ÁREA<br>80m<sup>2</sup> 
              </div>
              <div class="col-xs-4">
                PISOS<br>25 
              </div>
              <div class="col-xs-4">
                DPTOS.<br>250 
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4 c-mbot-sm">
          <div class="c-proy-otro">
            <a href="proyectos-entregados-detalle.php"><figure><img src="img/proy-entre1.png" alt=""></figure></a>
            <div class="c-proy-text">
              <a href="proyectos-entregados-detalle.php" class="c-h2 c-color-sklight c-titi-sem">Miramar</a>
              <p class="c-h4 c-color-sklight c-titi"><i>Av. del Pilar 1501 - Surco</i></p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat impedit placeat natus beatae ut, rerum maxime.</p>
            </div>
            <div class="row c-bg-sklight c-color-white c-feat-full text-center c-titi">
              <div class="col-xs-4">
                ÁREA<br>80m<sup>2</sup> 
              </div>
              <div class="col-xs-4">
                PISOS<br>25 
              </div>
              <div class="col-xs-4">
                DPTOS.<br>250 
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4 c-mbot-sm">
          <div class="c-proy-otro">
            <a href="proyectos-entregados-detalle.php"><figure><img src="img/proy-entre1.png" alt=""></figure></a>
            <div class="c-proy-text">
              <a href="proyectos-entregados-detalle.php" class="c-h2 c-color-sklight c-titi-sem">Miramar</a>
              <p class="c-h4 c-color-sklight c-titi"><i>Av. del Pilar 1501 - Surco</i></p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat impedit placeat natus beatae ut, rerum maxime.</p>
            </div>
            <div class="row c-bg-sklight c-color-white c-feat-full text-center c-titi">
              <div class="col-xs-4">
                ÁREA<br>80m<sup>2</sup> 
              </div>
              <div class="col-xs-4">
                PISOS<br>25 
              </div>
              <div class="col-xs-4">
                DPTOS.<br>250 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container text-center">
      <ul class="list-inline c-paginator c-titi">
        <li><a href="#"><i class="fa fa-caret-left"></i></a></li>
        <li><a href="#">1</a></li>
        <li><a href="#" class="activo">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">...</a></li>
        <li><a href="#">5</a></li>
        <li><a href="#"><i class="fa fa-caret-right"></i></a></li>
      </ul>
    </div>
  </section>

  
<?php include "includes/footer.php"; ?>  