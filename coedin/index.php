<?php include "includes/header.php"; ?>  

  <section>
    <div class="c-elastic">

      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-example-generic" data-slide-to="1"></li>
          <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
          <div class="item active">
            <div class="c-mbot-sm c-img-bgcover c-top-slide" style="background-image: url(img/slide1.jpg);">
              <div class="c-layer-fosc visible-xs"></div>
              <div class="c-box-text c-color-white">
                <p class="c-h2 c-text-shadow"><strong>NO HACEMOS CASAS</strong></p>
                <p class="c-h3">HACEMOS EL MEJOR LUGAR<br>PARA LA VIDA QUE MERECES</p>
                <a href="#" class="btn btn-lg c-btn-skfosc-white c-titi-sem">VER PROYECTOS</a>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="c-mbot-sm c-img-bgcover c-top-slide" style="background-image: url(img/actual-detalle.png);">
              <div class="c-layer-fosc visible-xs"></div>
              <div class="c-box-text c-color-white">
                <p class="c-h2 c-text-shadow"><strong>NO HACEMOS CASAS</strong></p>
                <p class="c-h3">HACEMOS EL MEJOR LUGAR<br>PARA LA VIDA QUE MERECES</p>
                <a href="#" class="btn btn-lg c-btn-skfosc-white c-titi-sem">VER PROYECTOS</a>
              </div>
            </div>
          </div>
          <div class="item">
            <div class="c-mbot-sm c-img-bgcover c-top-slide" style="background-image: url(img/proyectos-actuales.jpg);">
              <div class="c-layer-fosc visible-xs"></div>
              <div class="c-box-text c-color-white">
                <p class="c-h2 c-text-shadow"><strong>NO HACEMOS CASAS</strong></p>
                <p class="c-h3">HACEMOS EL MEJOR LUGAR<br>PARA LA VIDA QUE MERECES</p>
                <a href="#" class="btn btn-lg c-btn-skfosc-white c-titi-sem">VER PROYECTOS</a>
              </div>
            </div>
          </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>

      
      <div class="c-iblock-container ">
        <div class="c-iblock c-slide60 c-mbot-sm c-mder-xsr c-img-bgcover" style="background-image: url(img/proyectos-actuales-home.jpg);">
          <div class="c-box-text c-color-white">
            <a href="#" class="btn btn-lg c-btn-black-sk c-titi">PROYECTOS<span class="c-h4">ACTUALES</span></a>
          </div>
        </div>
        <div class="c-iblock c-slide40 c-mbot-sm c-img-bgcover" style="background-image: url(img/proyectos-entregados-home.jpg);">
          <div class="c-box-text c-color-white">
            <a href="#" class="btn btn-lg c-btn-black-green c-titi">PROYECTOS<span class="c-h4">ENTREGADOS</span></a>
          </div>
        </div>
      </div>
    </div>

    <div class="c-elastic">
      <div class="c-bg-blackt1 c-suscribe c-mbot-sm">
        <div class="container ">
          <span class="c-h4 c-lato-lig">Suscríbete a Nuestro Boletín</span>
          <form class="form-inline ">
            <div class="form-group">
              <input type="email" class="form-control" id="" placeholder="Déjanos tu email">
            </div>
            <button type="submit" class="btn c-bg-sklight c-titi-sem">Enviar</button>
          </form>
        </div>
      </div>
    </div>

    <div class="c-elastic">
      <div class="c-mbot-sm c-img-bgcover c-bot-slide" style="background-image: url(img/invierte-nosotros.jpg);">
        <div class="container">
          <div class="c-box-text c-color-white">
            <span class="c-h2 c-block c-titi">INVIERTE CON</span>
            <span class="c-h1xx c-titi-sem">COEDIN</span>
            <p class="c-mver-xs c-lato-lig"">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis assumenda alias totam nesciunt architecto quaerat eligendi recusandae autem.</p>
            <a href="#" class="btn btn-lg c-btn-sklight-white btn-wide c-titi">INVIERTE<br><span class="c-h4 c-titi-sem">AHORA</span></a>
          </div>
        </div>
      </div>
    </div>
    
  </section>

  <section>
    <div class="container">
      <h2 class="text-center c-titi-sem">NOTICIAS RELACIONADAS</h2>
      <div class="row c-mtop-sm">
        <div class="col-sm-6 col-md-3 c-mbot-sm">
          <div class="c-notihome c-notipo1">
            <figure><img src="img/ft1.png" alt=""></figure>
            <div class="c-noti-text c-bg-graylight">
              <p><small>19 Julio, 2016</small></p>
              <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
              <div class="clearfix">
                <a href="noticias.php" class="btn c-bor-gray pull-right">Leer más</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 c-mbot-sm">
          <div class="c-notihome c-notipo2 c-img-bgcover" style="background-image: url(img/ft2.png);">
            <div class="c-noti-text c-color-blackt1 c-text-shadow2">
              <p><small>19 Julio, 2016</small></p>
              <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
              <div class="clearfix">
                <a href="noticias.php" class="btn c-bor-gray pull-right">Leer más</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 c-mbot-sm">
          <div class="c-notihome c-notipo1">
            <figure><img src="img/ft3.png" alt=""></figure>
            <div class="c-noti-text c-bg-graylight">
              <p><small>19 Julio, 2016</small></p>
              <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
              <div class="clearfix">
                <a href="noticias.php" class="btn c-bor-gray pull-right">Leer más</a>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-3 c-mbot-sm">
          <div class="c-notihome c-notipo2 c-img-bgcover c-color-white" style="background-image: url(img/ft4.png);">
            <div class="c-layer-fosc"></div>
            <div class="c-noti-text ">
              <p><small>19 Julio, 2016</small></p>
              <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
              <div class="clearfix">
                <a href="noticias.php" class="btn c-bor-white pull-right">Leer más</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
<?php include "includes/footer.php"; ?>  