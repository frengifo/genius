<?php include "includes/header.php"; ?>  

  <section>
    <div class="container">
      <div class="text-center">
        <h1 class="c-h2 c-titi-sem">INVIERTE CON COEDÍN</h1>
        <p>Si estas interesado en nuestros proyectos, para  enviarte la información requerida formulario para poder enviarte.</p>
      </div>

      <div class="row c-row-centered c-mtop-sm">
        <div class="col-sm-12 col-md-8 c-col-centered">
          <form method="post" action="" class="c-form-invierte c-bg-blackb1 text-left">
            <div class="form-group c-mtop-xxs">
              <p class="c-h3">INVERSIONISTA</p>
              <label class="radio-inline">
                <input type="radio" name="inversionista" id="inlineRadio1" value="option1"> Persona
              </label>
              <label class="radio-inline">
                <input type="radio" name="inversionista" id="inlineRadio2" value="option2"> Empresa
              </label>
            </div>
            <div class="row c-mtop-xxs">
              <div class="form-group col-sm-6">
                <input type="text" class="form-control" id="" placeholder="Nombres">
              </div>
              <div class="form-group col-sm-6">
                <input type="email" class="form-control" placeholder="Email">
              </div>
              <div class="form-group col-sm-6">
                <input type="text" class="form-control" id="" placeholder="Apellidos">
              </div>
              <div class="form-group col-sm-6">
                <input type="text" class="form-control" id="" placeholder="Teléfono">
              </div>
            </div>
            <div class="row c-mtop-xxs">
              <div class="col-sm-6">
                <div class="form-group">
                  <p class="c-h3">TIPO DE INVERSIÓN</p>
                  <label class="radio-inline">
                    <input type="radio" name="inversionista" id="inlineRadio1" value="option1"> Persona
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="inversionista" id="inlineRadio2" value="option2"> Empresa
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="inversionista" id="inlineRadio2" value="option2"> Empresa
                  </label>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="" placeholder="Otro">
                </div>
              </div>

              <div class="col-sm-6">
                <div class="form-group">
                  <p class="c-h3">PROYECTO</p>
                  <p>Lorem ipsum dolor sit amet, consectetur *</p>
                </div>
                <div class="form-group">
                  <select name="" id="" class="form-control">
                    <option value="">Selecciona un proyecto</option>
                    <option value="">Miramar</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="row c-mtop-xxs">
              <div class="col-sm-12">
                <div class="form-group">
                  <p class="c-h3">DEJE SU MENSAJE</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit *</p>
                </div>
                <div class="form-group">
                  <textarea name="" id="" rows="7" class="form-control"></textarea>
                </div>
              </div>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox"> <small>Acepto recibir información de los proyectos de Codeín y haber leído y aceptado los Términos y Condiciones, y Políticas de Privacidad.</small>
              </label>
            </div>
            <div class="form-group clearfix">
              <button type="submit" class="btn c-bg-sklight c-color-white c-titi-sem pull-right">ENVIAR</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>

  
<?php include "includes/footer.php"; ?>  