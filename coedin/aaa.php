<?php get_header(); ?>
<style type="text/css">
    
    .offers, .brands, .video{
        display:  none;
    }
    html{
        display: block !important;
    }
    .fondo{
        background-position: 0 100%;
    }
    form{
        min-height: 400px;
    }
    form .new {
        display: none;
    }
    form div.msg-success{
        text-align: center;
    }
    form div.msg-success h3{
        font-size:  2em
    }
    form div.msg-success a{
        color: #fff;
        display: inline-block;
        text-align: center;
        font-size: 1.3em;
        font-weight: 900;
        margin: 0 auto;
        padding: .5em 1em;
        background-color: rgba(48, 48, 48, 0.85);
        border-radius: 7px;
        text-transform: uppercase;
    }
    .offers, .brands{
        display: none;
    }
    .offers:first-child,.brands:first-child{
        display: block;
    }
</style>
<div class="contenido fondo" style="background-image:url('<?php echo get_template_directory_uri() ?>/img/fondo.png');">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <form data-parsley-validate="" id="contactForm" >

                    <h1>Únete al Club Safetypay:</h1>
                    
                    <label>Nombre y Apellido</label>
                    <input type="text" name="name" data-parsley-pattern="^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]+$" required/>

                    <label>Edad</label>
                    <input type="text" data-parsley-range="[18, 99]" minlength="2" maxlength="2" data-parsley-type="digits" name="edad" data-parsley-type="digits" required/>

                    <label class="show">E-Mail</label>
                    <input type="email" name="email" class="show" required/>

                    <label>Celular</label>
                    <input type="text" name="tel" maxlength="20" data-parsley-type="digits" minlength="6" required/>

                    <label>País</label>
                    
										<select name="country">
                      <option value="mexico">México</option>
                      <option value="colombia">Colombia</option>
                      <option value="chile">Chile</option>
                      <option value="ecuador">Ecuador</option>
                      <option value="peru">Perú</option>
                    </select>
                    <div class="chk-terms">
                        <label for="terms">
                            <input type="checkbox" id="terms" name="terms" required /><a href="#">Acepto términos y condiciones</a>
                        </label>
                    </div>
                    <button type="submit">Enviar</button>
                    <p>
                        <a href="javascript:;" style="color:#fff;" class="alredy">Ya soy usuario</a>
                        <a href="javascript:;" style="color:#fff;" class="new">Registrarme</a>
                    </p>
                    
                    <div class="msg-success" style="display:none;"><h3>Gracias por registrarte.</h3></div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $paises = get_terms( 'pais', array( 'hide_empty' => 0 ) );
    if ( ! empty( $paises ) && ! is_wp_error( $paises ) ){
        
        foreach ( $paises as $pais ) { ?>
            <div class="offers <?php echo $pais->slug; ?>">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="color-azul">Oferta del Momento</h2>
                            <div class="slider">
                                <ul>
                                    <?php $slider = new WP_Query( array(  'post_type' => 'banner', 'pais' => $pais->slug ) ); ?> 
                                    <?php while ( $slider->have_posts() ) : $slider->the_post(); ?>               
                                    <li>
                                        <a href="<?php the_field('link') ?>" target="_blank">
                                            <img src="<?php the_field('imagen') ?>" alt="<?php the_title() ?>" />
                                        </a>
                                    </li>
                                    <?php endwhile; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } 
    }
    ?>
<?php 
    if ( ! empty( $paises ) && ! is_wp_error( $paises ) ){
        
        foreach ( $paises as $pais ) { ?>
            <div class="brands <?php echo $pais->slug; ?>">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 heading">
                            <h2 >Marcas Afiliadas</h2>
                        </div>  
                        <div class="clear"></div>
                        <?php 

                        $tipos = get_terms( 'marcas', array( 'hide_empty' => 0 ) );
                        $i=0;
                         if ( ! empty( $tipos ) && ! is_wp_error( $tipos ) ){
                            
                            foreach ( $tipos as $term ) { ?>

                                <?php if ( $term->count > 0): ?>
                                    
                                
                                    <article class="col-md-12">
                                        <div class="row">
                                            
                                            <div class="col-md-2 box">
                                                <a href="#" class="brand">
                                                    <span><img src="<?php the_field('logo', $term); ?>" alt="<?php echo $term->name; ?>"></span>
                                                </a>
                                            </div>

                                            <div class="col-md-10 box">

                                                <div class="row">
                                                <?php $ofertas = new WP_Query( array(  'post_type' => 'ofertas', 'marcas'=>$term->slug, 'pais' => $pais->slug

                                                         ) ); ?> 
                                                <?php while ( $ofertas->have_posts() ) : $ofertas->the_post(); ?>               
                                                        <div class="col-md-4 col-sm-4 col-xs-12 off">
                                                            <a href="<?php the_field('link') ?>" target="_blank">
                                                                <img src="<?php the_field('imagen') ?>" alt="<?php the_title() ?>" />
                                                            </a>
                                                        </div>  
                                                <?php endwhile; ?>                    

                                                </div>
                                            </div>

                                        </div>
                                    </article>
                                <?php endif ?>
                        <?php 
                            } 
                         }

                        ?>
                    </div>
                    <div style="color: #2C4E79;text-align:center;">Aplican t&eacute;minos y condiciones. El tipo de cambio mostrado para cada promoci&oacute;n es referencial y le pertenece a cada uno de los comercios asociados. Las ofertas presentes en este sitio web se encuentran sujetas a disponibilidad y se encontraban vigentes al momento de su publicaci&oacute;n.</div>
                </div>
            </div>
    <?php } 
    }
?>
<div class="video">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>¿Cómo comprar con safetypay?</h2>
                <div class="clear"></div>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/QpxiUF7vHzo" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
