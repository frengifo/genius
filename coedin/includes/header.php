<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>COEDIN - Construcción y Ventas Inmobiliarias</title>
    <script src="//use.fontawesome.com/416dbba35a.js"></script>
    <!-- <link href="bower_components/bootstrap/dist/css/bootstrap.css" rel="stylesheet"> -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Lato:300,400,700|Titillium+Web:400,600,700" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
    <header>
      <div class="c-head c-bg-blackb1">
        <div class="container clearfix">
          <ul class="list-inline pull-left">
            <li><a href="https://www.facebook.com/Coedin/" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
            <li><a href="https://www.linkedin.com" target="_blank"><i class="fa fa-linkedin"></i></a></li>
          </ul>
          <ul class="list-inline pull-right">
            <li><a href="tel:987654321">Comunícate al <strong><i>98765321</i></strong></a></li>
          </ul>
        </div>
      </div>
      <div class="navbar-static-top c-header">
         <div class="container">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a href="/" class="c-logo"><img src="img/logo.png" alt="Triny Rent"></a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-ex-collapse">
               <ul class="nav navbar-nav navbar-right c-menu c-titi-sem">
                  <li class="active"><a href="proyectos-actuales.php">PROYECTOS ACTUALES</a></li>
                  <li><a href="proyectos-entregados.php">PROYECTOS ENTREGADOS</a></li>
                  <li><a href="invierte-en-coedin.php">INVIERTE CON COEDIN</a></li>
                  <li><a href="noticias.php">NOTICIAS</a></li>
                  <li><a href="contactanos.php">CONTACTO</a></li>
               </ul>
            </div>
         </div>
      </div>
    </header>