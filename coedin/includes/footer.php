    <footer class="c-bg-blackt1">
      <div class="container">
        <div class="row c-mtop-md">
          <div class="col-sm-6 col-md-3 c-mbot-md">
            <p class="c-h4 c-titi-sem">LA EMPRESA</p>
            <div class="c-text-justify c-mbot-xs c-lato-lig">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero esse eos culpa repellat, corrupti laboriosam nobis dolores nemo ea perspiciatis quam. Vel magni sed nesciunt nobis
            </div>
            <a href="#" class="c-link">LEER MÁS</a>
          </div>
          <div class="col-sm-6 col-md-3 c-mbot-md">
            <p class="c-h4 c-titi-sem">ATENCIÓN AL CLIENTE</p>
            <ul class="list-unstyled c-lato-lig">
              <li><a href="#">Términos y Condiciones</a></li>
              <li><a href="#">Políticas de Privacidad</a></li>
            </ul>
            <p class="c-h4 c-titi-sem c-mtop-sm">TIPOS DE INVERSIÓN</p>
            <ul class="list-unstyled c-lato-lig">
              <li><a href="#">Venta de terrenos</a></li>
              <li><a href="#">Compra de casas y departamentos</a></li>
            </ul>
          </div>
          <div class="col-sm-6 col-md-3 c-mbot-md">
            <div class="c-foot-invierte">
              <p class="c-h4 c-color-sklight c-titi">INVIERTE CON <br><span class="c-h2">COEDIN</span></p>
              <div class="c-text-justify c-lato-lig">
                <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero esse eos culpa repellat.</small>
              </div>
              <p class="c-mtop-xs"><a href="#" class="btn btn-lg btn-block c-bg-sklight c-titi">INVIERTE<br><span class="c-h4 c-titi-sem">AHORA</span></a></p>
            </div>
          </div>
          <div class="col-sm-6 col-md-3 c-mbot-md c-fdatos">
            <p class="c-h4 c-titi-sem">CONTÁCTANOS</p>
            <ul class="list-unstyled c-lato-lig">
              <li><a href="https://goo.gl/maps" target="_blank"><i class="fa fa-map-marker"></i> Av. Berlín 1501 Of. 102<br>Miraflores, Lima - Perú</a></li>
              <li><a href="mailto:informes@coedin.com"><i class="fa fa-envelope"></i> informes@coedin.com</a></li>
            </ul>
            <p class="c-h4 c-titi-sem c-mtop-sm">SÍGUENOS EN</p>
            <div class="row c-redes c-mtop-xs">
              <div class="col-xs-4">
                <a href="https://www.facebook.com" target="_blank"><i class="fa fa-facebook"></i> </a>
              </div>
              <div class="col-xs-4">
                <a href="#"><i class="fa fa-youtube"></i> </a>
              </div>
              <div class="col-xs-4">
                <a href="#"><i class="fa fa-linkedin"></i> </a>
              </div>

            </div>
          </div>
        </div>
      </div>

      <div class="c-foot c-bg-blackt1">
        <div class="container text-center">
          <small class="c-color-sklight">Copyright &copy; 2016 <strong>Coedin</strong> | Diseñado y Desarrollado por <strong>Reder Design</strong></small>
        </div>
      </div>
    </footer>

    <script type="text/javascript" src="js/jquery.slim.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://npmcdn.com/masonry-layout@4.1/dist/masonry.pkgd.min.js"></script>
    <script type="text/javascript">
      $(function() {
        $('.c-menu a').each(function() {
          var word = $(this).html();
          var index = word.indexOf(' ');
          if(index > -1) {
            // index = word.length;
            $(this).html(word.substring(0, index) + '<br>' + word.substring(index, word.length));
          }
        });

        $('.carousel').carousel({
          interval: 7000
        })
        /**/
        $('#miniCarousel').carousel({
          interval: 10000
        });
        //Handles the carousel thumbnails
        $('[id^=carousel-selector-]').click( function(){
            var id = this.id.substr(this.id.lastIndexOf("-") + 1);
            var id = parseInt(id);
            $('#miniCarousel').carousel(id);
        });
 
      });
    </script>
    <script type="text/javascript">
      
      jQuery(document).ready(function(){

          $('.c-mtop-sm').masonry({
            // options...
            itemSelector: '.c-mbot-sm',
            columnWidth: '.grid-sizer',
            percentPosition: true
          });

      })

  </script>
  </body>
</html>