<?php include "includes/header.php"; ?>  

  <section>
    <div class="c-mbot-sm">
      <div id="map" class="c-map2"></div>
    </div>
  </section>

  <section>
    <div class="container">

      <div class="row c-row-centered c-mtop-sm">
        <div class="col-sm-10 col-md-8 col-lg-6 c-col-centered">
          <form method="post" action="" class="text-left">
            <div class="form-group">
              <input type="email" class="form-control" placeholder="Email">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="" placeholder="Teléfono">
            </div>
            <div class="form-group">
              <textarea name="" id="" rows="7" class="form-control" placeholder="Mensaje"></textarea>
            </div>
            <div class="checkbox">
              <label>
                <input type="checkbox"> <small>Deseo subscribirme al boletín y recibir información de los proyectos de Codeín</small>
              </label>
            </div>
            <div class="form-group clearfix">
              <button type="submit" class="btn c-bg-sklight c-color-white c-titi-sem">ENVIAR</button>
            </div>
          </form>
        </div>
        <div class="col-sm-10 col-md-3 col-lg-3 c-col-centered c-contactinfo text-left">
          <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-12">
              <h3 class="c-h3 c-titi-sem hidden-md hidden-lg c-mbot-xs">CONTACTO</h3>
              <ul class="list-unstyled">
                <li><a href="tel:14561327"><i class="fa fa-phone"></i> (01) 456 1327</a></li>
                <li><a href="tel:16359745"><i class="fa fa-phone"></i> (01) 635 9745</a></li>
                <li><a href="mailto:informes@coedin.com"><i class="fa fa-envelope"></i> informes@coedin.com</a></li>
                <li><a href="https://goo.gl/maps" target="_blank"><i class="fa fa-map-marker"></i> Av. Berlín 1501 Of. 102<br>Miraflores, Lima - Perú</a></li>
              </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-12 c-mbot-sm">
              <h3 class="c-h3 c-titi-sem c-mbot-xs">SÍGUENOS EN</h3>
              <div class="c-follow c-mbot-xs">
                <a href="#" target="_blank"><img src="img/btn-facebook" alt=""></a>
              </div>
              <div class="c-follow c-mbot-xs">
                <a href="#" target="_blank"><img src="img/btn-youtube" alt=""></a>
              </div>
              <div class="c-follow c-mbot-xs">
                <a href="#" target="_blank"><img src="img/btn-link" alt=""></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<script>
  function initMap() {
    var myLatLng = {lat: -12.1275186, lng: -76.9753165};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: myLatLng
    });
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: "img/marker.png",
      title: 'Visitanos',
      animation: google.maps.Animation.DROP,
    });
  }
</script>
<script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6w1tQpuDMZffD-cPP6y5cTXw42rHF_Ag&callback=initMap">
</script>

<?php include "includes/footer.php"; ?>  