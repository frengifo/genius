<?php include "includes/header.php"; ?>  

  <section>
    <div class="c-elastic">
      <div class="c-mbot-sm c-img-bgcover c-coverslide" style="background-image: url(img/proyectos-actuales.jpg);">
        <div class="c-box-text c-color-blackt1">
          <p class="c-h2 c-titi-bol">TENEMOS EL LUGAR<br>QUE SIEMPRE ESPERASTE</p>
          <p class="c-h3 c-titi">CONOCE NUESTROS<br><span class="c-titi-sem">PROYECTOS ACTUALES</span></p>
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="row ">

        <div class="col-sm-12 col-md-8 c-mbot-sm">
            <figure><img src="img/los-molinos-profile.jpg" alt=""></figure>
        </div>
        <div class="col-sm-12 col-md-4  text-left">
          <div class="c-infoproy">
            <span class="c-h1 c-block c-titi-sem">Los Molinos</span>
            <p class="c-h3 c-titi-sem">Surco</p>
            <div class="c-desc">
              <p>Las proporciones de los ambientes han lorem a sido analizadas para pri las necesidades de comodidad, elegancia independencia y funcionalidad para vivir.</p>
              <p>Proporciones de los ambientes para pri las necesidades de comodidad, independencia y a la de funcionalidad para vivir en equilibrio.</p>
            </div>
            <div class="c-features c-color-sklight c-titi-sem text-center c-mtop-sm c-mbot-sm">
              <div class="c-iblock-container">
                <div class="c-iblock">
                  ÁREA<br>80m<sup>2</sup> 
                </div>
                <div class="c-iblock">
                  PISOS<br>25 
                </div>
                <div class="c-iblock">
                  DPTOS.<br>250 
                </div>
              </div>
            </div>
            <div class="text-center">
              <a href="proyectos-actuales-detalle.php" class="btn c-bg-sklight c-color-white">VER MÁS</a>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>

  <section>
    <div class="container c-proyslide">
      <a href="#">
        <figure class="c-proyslide-img">
          <img src="img/los-molinos-profile.jpg" alt="">
          <figcaption class="c-mask-skylight text-center c-color-white">
            <div class="c-mask-text">
              <p class="c-h2 c-titi-bol">Los Prados</p>
              <p class="c-h4 c-titi">San Borja</p>
            </div>
          </figcaption>
        </figure>
      </a>
    </div>
  </section>
  
<?php include "includes/footer.php"; ?>  