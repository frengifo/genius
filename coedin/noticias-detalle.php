<?php include "includes/header.php"; ?>  

  <section>
    <div class="container">
      <div class="c-mbot-sm c-img-bgcover c-coverslide" style="background-image: url(img/noti-detalle.png);">
      </div>
    </div>

    <div class="container c-notideta">
      <div class="row">
        <div class="col-md-9">
          <h1>NUEVA VISTA</h1>
          <hr class="c-hr">
          <div class="clearfix c-inline-float c-mbot-sm text-center">
            <div class="c-share c-color-white">
              <div class="c-face"><i class="fa fa-facebook"></i></div>
              <div class="c-twit"><i class="fa fa-twitter"></i></div>
              <div class="c-goog"><i class="fa fa-google-plus"></i></div>
            </div>
            <div class="c-autor c-mtop-xs">
              Por <span class="c-color-sklight">Andrea Muñoz</span> <span class="c-separador">|</span> 24 de Mayo, 2016
            </div>
          </div>
          <div class="c-text">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et quo obcaecati, iusto facere asperiores ut nulla dolores itaque hic dignissimos, tempora suscipit aliquam magnam amet quos. Ex et, mollitia eum.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur cumque sunt ea omnis est officia, a molestias mollitia hic quibusdam optio? Optio, doloribus quia est quis quod dolorum iusto esse.</p>
          </div>

          <figure class="c-mtop-sm">
            <img src="img/noti1.png" alt="">
            <figcaption>
              <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptates facere architecto praesentium.</small>
            </figcaption>
          </figure>

          <div class="c-text">
            <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas odit repellat vitae mollitia, enim debitis, vel sed optio laboriosam fugiat, nisi quia fuga esse nihil, at vero beatae quam? Vel.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure asperiores, quod, reiciendis quae quidem atque natus aliquid laudantium consectetur numquam sapiente sint, impedit enim totam illum aliquam officiis nam molestiae.</p>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga vitae laboriosam cum rerum debitis adipisci eos natus error eum sint, dolorem aliquid voluptatibus modi doloribus maxime aperiam, quasi magni delectus! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempora vel cumque temporibus ullam libero animi corrupti non, consequatur laboriosam alias praesentium delectus facilis nihil ducimus modi neque voluptate voluptatum voluptatem.</p>
          </div>

          <div class="clearfix c-share-bottom">
            <div class="c-share text-center c-color-white c-block c-mtop-sm c-mbot-md">
              <div class="c-face"><i class="fa fa-facebook"></i></div>
              <div class="c-twit"><i class="fa fa-twitter"></i></div>
              <div class="c-goog"><i class="fa fa-google-plus"></i></div>
            </div>
          </div>

          <div class="c-autorinfo">
            <div class="row">
              <div class="col-sm-2 col-md-2 c-mbot-xs c-userpic">
                <figure><img src="img/user1.png" alt=""></figure>
              </div>
              <div class="col-sm-10 col-md-10 c-mbot-xs">
                <span class="c-h4 c-titi-sem c-block">Javier Guzmán</span>
                <span class="c-mbot-xs c-block">13 de Junio, 2016</span>
                <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum minima vel delectus hic officiis, sapiente, molestiae maxime nihil eos quis veniam illo, velit ratione suscipit ut. Doloribus facere laborum itaque.</small></p>
              </div>
            </div>
          </div>

          <div class="c-comentarios c-mtop-md">
            <h3 class="c-h1 c-titi-sem c-mbot-xs">Comentarios</h3>
            <div class="row">
              <div class="col-sm-2 col-md-2 c-mbot-xs c-userpic">
                <figure><img src="img/user2.png" alt=""></figure>
              </div>
              <div class="col-sm-10 col-md-10 c-mbot-sm">
                <span class="c-h4 c-titi-sem c-block">Alberto Barros</span>
                <span class="c-mbot-xs c-block">13 de Junio, 2016</span>
                <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum minima vel delectus hic officiis, sapiente, molestiae maxime nihil eos quis veniam illo, velit ratione suscipit ut. Doloribus facere laborum itaque.</small></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-2 col-md-2 c-mbot-xs c-userpic">
                <figure><img src="img/user2.png" alt=""></figure>
              </div>
              <div class="col-sm-10 col-md-10 c-mbot-sm">
                <span class="c-h4 c-titi-sem c-block">Alberto Barros</span>
                <span class="c-mbot-xs c-block">13 de Junio, 2016</span>
                <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum minima vel delectus hic officiis, sapiente, molestiae maxime nihil eos quis veniam illo, velit ratione suscipit ut. Doloribus facere laborum itaque.</small></p>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-2 col-md-2 c-mbot-xs c-userpic">
                <figure><img src="img/user2.png" alt=""></figure>
              </div>
              <div class="col-sm-10 col-md-10 c-mbot-sm">
                <span class="c-h4 c-titi-sem c-block">Alberto Barros</span>
                <span class="c-mbot-xs c-block">13 de Junio, 2016</span>
                <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum minima vel delectus hic officiis, sapiente, molestiae maxime nihil eos quis veniam illo, velit ratione suscipit ut. Doloribus facere laborum itaque.</small></p>
              </div>
            </div>
          </div>
    
          <div class="c-comment c-mbot-md">
            <h3 class="c-h1 c-titi-sem c-mbot-xs">Déjanos tu comentario</h3>
            <div class="row">
              <div class="hidden-xs col-sm-2 col-md-2 c-mbot-xs c-userpic">
                <figure><img src="img/usern.jpg" alt=""></figure>
              </div>
              <div class="col-sm-10 col-md-10">
                <form>
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Nombre">
                  </div>
                  <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email">
                  </div>
                  <div class="form-group">
                   <textarea name="" id="" rows="7" class="form-control" placeholder="Comentario"></textarea>
                  </div>
                  <div class="form-group">
                    <p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe cumque quos quidem quasi.</small></p>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn c-bg-blackb1 c-color-white c-titi-sem pull-right">COMENTAR</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

        </div>

        <div class="col-md-3">
          <h3 class="c-h1 c-titi-sem hidden-md hidden-lg">Buscar</h3>
          
          <div class="c-buscar">
            <form class="">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Buscar Noticias">
                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                </div>
              </div>
            </form>
          </div>
          <hr class="c-hr">
  
          <div class="row c-mtop-sm">

            <div class="col-sm-6 col-md-12 c-mbot-sm">
              <div class="c-notihome c-notipo1">
                <figure><img src="img/noti-mas.png" alt=""></figure>
                <div class="c-noti-text c-bg-graylight">
                  <p><small>19 Julio, 2016</small></p>
                  <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
                  <div class="clearfix">
                    <a href="noticias-detalle.php" class="btn c-bor-gray pull-right">Leer más</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-12 c-mbot-sm">
              <div class="c-notihome c-notipo1">
                <figure><img src="img/noti-mas.png" alt=""></figure>
                <div class="c-noti-text c-bg-graylight">
                  <p><small>19 Julio, 2016</small></p>
                  <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
                  <div class="clearfix">
                    <a href="noticias-detalle.php" class="btn c-bor-gray pull-right">Leer más</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-12 c-mbot-sm">
              <div class="c-notihome c-notipo1">
                <figure><img src="img/noti-mas.png" alt=""></figure>
                <div class="c-noti-text c-bg-graylight">
                  <p><small>19 Julio, 2016</small></p>
                  <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
                  <div class="clearfix">
                    <a href="noticias-detalle.php" class="btn c-bor-gray pull-right">Leer más</a>
                  </div>
                </div>
              </div>
            </div>


          </div>

        </div>
      </div>
    </div>
  </section>
  
<?php include "includes/footer.php"; ?>  