<?php include "includes/header.php"; ?>  
  <style type="text/css">
    
  .grid-sizer{
    width: 25%;
  }
  .c-mbot-sm{
    height: 580px;
  }
  .c-notihome {
    position: relative;
    height: 100%;
    display: block;
    background: #f4f4f4;
    width: 100%;
  }
  .c-notihome .c-noti-text {
    position: absolute;
    z-index: 2;
    padding: 1em;
    width: 100%;
    left: 0;
    bottom: 0;
  }
  @media screen and (max-width: 1200px) {
    .c-mbot-sm{
      font-size: 1em;
      height: 480px;
    }
  }
  @media screen and (max-width: 768px) {
    /* 5 columns for larger screens */
    .grid-sizer { width: 50%; }
    .c-mbot-sm{
      height: 430px;
    }
  }

   @media screen and (max-width: 480px) {
    /* 5 columns for larger screens */
    .grid-sizer, .c-mbot-sm { width: 100%; }
    .c-mbot-sm{
      height: 400px;
    }
  }
  </style>
  
  <section>
    <div class="container">
      <div class="row c-mtop-sm" style="position:relative;">
        <div class="grid-sizer"></div>
        <div class="col-sm-6 col-md-3 c-mbot-sm col-xs-6">
          <div class="c-notihome c-notipo1">
            <figure><img src="img/ft1.png" alt=""></figure>
            <div class="c-noti-text c-bg-graylight">
              <p><small>19 Julio, 2016</small></p>
              <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
              <div class="clearfix">
                <a href="noticias-detalle.php" class="btn c-bor-gray pull-right">Leer más</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-md-3 c-mbot-sm col-xs-6">
          <div class="c-notihome c-notipo2 c-img-bgcover" style="background-image: url(img/ft2.png);">
            <div class="c-noti-text c-color-blackt1 c-text-shadow2">
              <p><small>19 Julio, 2016</small></p>
              <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
              <div class="clearfix">
                <a href="noticias-detalle.php" class="btn c-bor-gray pull-right">Leer más</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-md-3 c-mbot-sm col-xs-6">
          <div class="c-notihome c-notipo1">
            <figure><img src="img/ft3.png" alt=""></figure>
            <div class="c-noti-text c-bg-graylight">
              <p><small>19 Julio, 2016</small></p>
              <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
              <div class="clearfix">
                <a href="noticias-detalle.php" class="btn c-bor-gray pull-right">Leer más</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-md-3 c-mbot-sm col-xs-6">
          <div class="c-notihome c-notipo3">
            <div class="c-noti-text c-noti-tipo4 c-bg-graylight">
              <p><small>19 Julio, 2016</small></p>
              <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio</p>
              <div class="clearfix">
                <a href="noticias-detalle.php" class="btn c-bor-gray pull-right">Leer más</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-md-6 c-mbot-sm col-xs-6">
          <div class="c-notihome c-notipo1">
            <figure><img src="img/somos.png" alt=""></figure>
            <div class="c-noti-text c-bg-graylight">
              <p><small>19 Julio, 2016</small></p>
              <h4>SOMOS PARTE DE TUS SUEÑOS</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque laboriosam tempore, placeat soluta eveniet</p>
              <div class="clearfix">
                <a href="noticias-detalle.php" class="btn c-bor-gray pull-right">Leer más</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-md-3 c-mbot-sm col-xs-6">
          <div class="c-notihome c-notipo1">
            <figure><img src="img/ft1.png" alt=""></figure>
            <div class="c-noti-text c-bg-graylight">
              <p><small>19 Julio, 2016</small></p>
              <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
              <div class="clearfix">
                <a href="noticias-detalle.php" class="btn c-bor-gray pull-right">Leer más</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-md-3 c-mbot-sm col-xs-6">
          <div class="c-notihome c-notipo2 c-img-bgcover c-color-white" style="background-image: url(img/ft4.png);">
            <div class="c-layer-fosc"></div>
            <div class="c-noti-text ">
              <p><small>19 Julio, 2016</small></p>
              <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>

              <div class="clearfix">
                <a href="noticias-detalle.php" class="btn c-bor-white pull-right">Leer más</a>
              </div>
            </div>
          </div>
        </div>
        
        <div class="col-sm-6 col-md-3 c-mbot-sm col-xs-6">
          <div class="c-notihome c-notipo1">
            <figure><img src="img/ft5.png" alt=""></figure>
            <div class="c-noti-text c-bg-graylight">
              <p><small>19 Julio, 2016</small></p>
              <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
              <div class="clearfix">
                <a href="noticias-detalle.php" class="btn c-bor-gray pull-right">Leer más</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-md-3 c-mbot-sm col-xs-6">
          <div class="c-notihome c-notipo2 c-img-bgcover c-color-white" style="background-image: url(img/ft4.png);">
            <div class="c-layer-fosc"></div>
            <div class="c-noti-text ">
              <p><small>19 Julio, 2016</small></p>
              <h4>LO ÚLTIMO PARA CASAS DE PLAYA</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
              <div class="clearfix">
                <a href="noticias-detalle.php" class="btn c-bor-white pull-right">Leer más</a>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-6 col-md-6 c-mbot-sm col-xs-6">
          <div class="c-notihome c-notipo1">
            <figure><img src="img/ft6.png" alt=""></figure>
            <div class="c-noti-text c-bg-graylight">
              <p><small>19 Julio, 2016</small></p>
              <h4>SOMOS PARTE DE TUS SUEÑOS</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore velit labore repellendus earum omnis voluptates distinctio ...</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque laboriosam tempore, placeat soluta eveniet</p>
              <div class="clearfix">
                <a href="noticias-detalle.php" class="btn c-bor-gray pull-right">Leer más</a>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>

  
<?php include "includes/footer.php"; ?>  