<?php include "includes/header.php"; ?>  

  <section>
    <div class="c-elastic">
      <div class="c-mbot-sm c-img-bgcover c-coverslide" style="background-image: url(img/actual-detalle.png);">
      </div>
    </div>
  </section>

  <section>
    <div class="container">
      <div class="clearfix c-idioma">
        <div class="pull-right">
          <span class="c-titi-bol">Idioma:</span> <a href="#" class="activo">Español </a> <span class="c-titi-sem">|</span> <a href="#">Inglés</a>
        </div>
      </div>

      <div class="row c-proy-deta c-mtop-sm">
        <div class="col-md-5 c-mbot-sm">
          <h1 class="c-titi-sem">Los Molinos</h1>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sint necessitatibus, deleniti ipsum totam amet ullam quam iusto aliquid, magnam expedita! Amet distinctio dolor reiciendis culpa quae rerum qui quo doloribus.</p>
          <ul class="">
            <li>88% DE DPTOS CON VISTA AL MAR</li>
            <li>CENTRAL DE SERENAZGO</li>
            <li>CÁMARAS DE SEGURIDAD</li>
            <li>PSICINA CON VISTA AL MAR</li>
            <li>FACIL ACCESO, TRANSPORTE PÚBLICO</li>
            <li>PRECIO ACCESIBLE</li>
            <li>MALECÓN VERDE</li>
            <li>VENTANA PANORÁMICA</li>
          </ul>
        </div>
        <div class="col-md-7 c-mbot-sm">
          <div class="row">
            <div class="col-sm-2">
              <div class="row c-item-vert">
                <div class="col-xs-6 col-sm-12 c-mbot-xs"><figure><img src="img/proyitem1.png" alt=""></figure></div>
                <div class="col-xs-6 col-sm-12 c-mbot-xs"><figure><img src="img/proyitem1.png" alt=""></figure></div>
                <div class="col-xs-6 col-sm-12 c-mbot-xs"><figure><img src="img/proyitem1.png" alt=""></figure></div>
                <div class="col-xs-6 col-sm-12 c-mbot-xs"><figure><img src="img/proyitem1.png" alt=""></figure></div>
                <div class="col-xs-6 col-sm-12 c-mbot-xs"><figure><img src="img/proyitem1.png" alt=""></figure></div>
              </div>
            </div>
            <div class="col-sm-10">
              <figure><img src="img/los-molinos.jpg" alt=""></figure>
            </div>
          </div>
        </div>
      </div>

    </div>
  </section>

  <section>
    <div class="container">

      <div class="c-tipo-box">
         <div class="row">
           <div class="col-sm-6">
             <div class="carousel slide" id="miniCarousel">
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="active item" data-slide-number="0">
                      <img src="img/plano.jpg">
                    </div>
                    <div class="item" data-slide-number="1">
                      <img src="img/depa.jpg">
                    </div>
                    <div class="item" data-slide-number="2">
                      <img src="img/ft6.png">
                    </div>
                    <div class="item" data-slide-number="3">
                      <img src="img/proyitem1.png">
                    </div>
                </div>
                <!-- Carousel nav -->
                <div class="c-minicontrol">
                  <a class="left carousel-control" href="#miniCarousel" role="button" data-slide="prev">
                    <i class="fa fa-angle-left"></i></a>
                  <a class="right carousel-control" href="#miniCarousel" role="button" data-slide="next">
                      <i class="fa fa-angle-right"></i>                                       
                  </a> 
                </div>                               
            </div>
           </div>
           
           <div class="col-sm-6 c-mbot-sm">
            <h2 class="c-color-sklight c-titi-sem">DEPARTAMENTO A</h2>
            <p class="c-titi-sem c-h4"><i>ÁREA TOTAL 63m<sup>2</sup></i></p>
            <div class="c-proy-desc">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Totam at quasi sapiente vel aut illo aperiam eius provident, consequuntur, soluta dolorem perferendis. Tempore minus, doloribus modi labore molestias nesciunt excepturi.</p>
            </div>
            <ul class="c-titi-sem">
              <li>2 Dormitorios</li>
              <li>1 Baño</li>
              <li>Cocina</li>
              <li>Sala</li>
              <li>Comedor</li>
              <li>Lavanderia</li>
            </ul>
           </div>
         </div>

         <div class="row c-carrusel c-mtop-sm">
          <div class="col-xs-6 col-sm-2 c-mbot-xs">
            <figure class="thumbnail" id="carousel-selector-0"><img src="img/plano.jpg" alt=""></figure>
          </div>
          <div class="col-xs-6 col-sm-2 c-mbot-xs">
            <figure class="thumbnail" id="carousel-selector-1"><img src="img/depa.jpg" alt=""></figure>
          </div>
          <div class="col-xs-6 col-sm-2 c-mbot-xs">
            <figure class="thumbnail" id="carousel-selector-1"><img src="img/ft6.png" alt=""></figure>
          </div>
          <div class="col-xs-6 col-sm-2 c-mbot-xs">
            <figure class="thumbnail" id="carousel-selector-1"><img src="img/proyitem1.png" alt=""></figure>
          </div>
         </div>

      </div>

        <div class="text-center c-mtop-xs c-mbot-sm">
          <a href="#" class="btn btn-lg c-bg-sklight c-titi-sem c-color-white">CONTACTAR AHORA</a>
        </div>
    </div>
  </section>

  <section>
    <div class="container">
      <p class="c-h2 text-center c-titi-sem">OTROS PROYECTOS</p>
      <div class="row c-mtop-xs">
        <div class="col-sm-4 c-mbot-sm">
          <div class="c-proy-otro">
            <a href="proyectos-entregados-detalle.php"><figure><img src="img/miramar.jpg" alt=""></figure></a>
            <div class="c-proy-text">
              <p class="c-h4 c-color-sklight c-titi"><i>Av. del Pilar 1501 - Surco</i></p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat impedit placeat natus beatae ut, rerum maxime.</p>
            </div>
            <div class="row c-bg-sklight c-color-white c-feat-full text-center c-titi">
              <div class="col-xs-4">
                ÁREA<br>80m<sup>2</sup> 
              </div>
              <div class="col-xs-4">
                PISOS<br>25 
              </div>
              <div class="col-xs-4">
                DPTOS.<br>250 
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4 c-mbot-sm">
          <div class="c-proy-otro">
            <a href="proyectos-entregados-detalle.php"><figure><img src="img/miramar.jpg" alt=""></figure></a>
            <div class="c-proy-text">
              <p class="c-h4 c-color-sklight c-titi"><i>Av. del Pilar 1501 - Surco</i></p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat impedit placeat natus beatae ut, rerum maxime.</p>
            </div>
            <div class="row c-bg-sklight c-color-white c-feat-full text-center c-titi">
              <div class="col-xs-4">
                ÁREA<br>80m<sup>2</sup> 
              </div>
              <div class="col-xs-4">
                PISOS<br>25 
              </div>
              <div class="col-xs-4">
                DPTOS.<br>250 
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4 c-mbot-sm">
          <div class="c-proy-otro">
            <a href="proyectos-entregados-detalle.php"><figure><img src="img/miramar.jpg" alt=""></figure></a>
            <div class="c-proy-text">
              <p class="c-h4 c-color-sklight c-titi"><i>Av. del Pilar 1501 - Surco</i></p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat impedit placeat natus beatae ut, rerum maxime.</p>
            </div>
            <div class="row c-bg-sklight c-color-white c-feat-full text-center c-titi">
              <div class="col-xs-4">
                ÁREA<br>80m<sup>2</sup> 
              </div>
              <div class="col-xs-4">
                PISOS<br>25 
              </div>
              <div class="col-xs-4">
                DPTOS.<br>250 
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php include "includes/footer.php"; ?>  